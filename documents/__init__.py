from sale import DocumentSale
from refund import DocumentRefund
from cashout import DocumentCashOut, DocumentCash
from cashin import DocumentCashIn
from move import DocumentMove
from inventory import DocumentInventory, DocumentInventoryBase
from invoice import DocumentInvoice

from PyQt4 import QtCore, QtGui


class Completer(QtGui.QCompleter):

    _model = None
    _limit = 10
    _field = ''
    _value = ''
    _max_length = 5
    _list = None

    def __init__(self, line_edit, model, field, limit=15, max_length=5):
        super(Completer, self).__init__()

        self.setModel(QtGui.QStringListModel())
        self.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.setCompletionMode(self.UnfilteredPopupCompletion)
        self.setMaxVisibleItems(limit)
        line_edit.setCompleter(self)
        line_edit.textEdited.connect(self.update)

        self._model = model
        self._field = field
        self._limit = limit
        self._max_length = max_length

    def update(self, value):
        new_value = unicode(value)
        old_value = self._value

        if new_value and self._list:
            strings = self._list.filter(new_value, self.caseSensitivity())
            self.model().setStringList(strings)

        elif not new_value:
            self._list = None

        elif not old_value and new_value and not self._list:
            filters = {
                '{}__icontains'.format(self._field): unicode(value)
            }
            query_set = self._model.objects.filter(
                **filters
            )
            query_set.query.group_by = [self._field]
            strings = query_set.values_list(self._field, flat=True)[:self._limit]

            self.model().setStringList(strings)
            self._list = self.model().stringList()

        self._value = new_value
