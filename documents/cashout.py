# coding: utf-8

from PyQt4 import QtCore, QtGui


from settings import settings
import models
import documents
import res


class DocumentCash(QtGui.QDialog):

    document = None
    total_sum = 0.0

    _notes = {
        500: 0, 200: 0, 100: 0, 50: 0,
        20: 0, 10: 0, 5: 0, 2: 0, 1: 0,
    }

    _coins = {
        50: 0, 25: 0, 10: 0,
        5: 0, 2: 0, 1: 0,
    }

    def __init__(self, parent, *args, **kwargs):
        super(DocumentCash, self).__init__(parent)

        ui = res.cashout.Ui_Dialog()
        ui.setupUi(self)

        self.ui = ui
        self.parent = parent
        self.document = kwargs.get('model')

        ui.groupBoxPrevious.setVisible(False)

        for key in self._notes:
            box = getattr(ui, 'spinBox_' + str(key))
            box.valueChanged.connect(self.update)

        for key in self._coins:
            box = getattr(ui, 'spinBoxCoin_' + str(key))
            box.valueChanged.connect(self.update)

        ui.buttonClose.clicked.connect(self.close)
        ui.buttonOk.clicked.connect(self.ok_clicked)
        ui.spinBox_500.setFocus()
        ui.spinBox_500.selectAll()

        self.completer = documents.Completer(self.ui.lineEditComment, self.model, field='comment')

    def get_notes(self):
        """Возвращает список купюр"""
        l = []
        for key in sorted(self._notes.iterkeys(), reverse=True):
            l.append(str(self._notes[key]))
        return l

    def get_coins(self):
        """Возвращает список монет"""
        l = []
        for key in sorted(self._coins.iterkeys(), reverse=True):
            l.append(str(self._coins[key]))
        return l

    def get_coins_sum(self):
        """Возвращает сумму мелочи в гривнах"""
        coins_sum = 0.0
        for key, value in self._coins.items():
            if not value:
                continue
            coins_sum += value * key * 0.01
        return coins_sum

    def get_notes_sum(self):
        """Возвращает сумму купюр"""
        notes_sum = 0
        for key, value in self._notes.items():
            if not value:
                continue
            notes_sum += value * key
        return notes_sum

    def load(self, document=None):

        document = document or self.document

        if not document.pk:
            return

        ui = self.ui
        l = document.notes.split(',')
        for key in sorted(self._notes.iterkeys()):
            value = int(l.pop())
            box = getattr(ui, 'spinBox_' + str(key))
            box.setValue(value)

        l = document.coins.split(',')
        for key in sorted(self._coins.iterkeys()):
            value = int(l.pop())
            box = getattr(ui, 'spinBoxCoin_' + str(key))
            box.setValue(value)

        ui.lineEditComment.setText(document.get_comment())

        if self.document.pk and not document.is_active:
            self.ui.buttonOk.setDisabled(True)
            self.ui.lineEditComment.setReadOnly(True)

    def update(self):
        ui = self.ui

        coins_sum = 0
        notes_sum = 0

        for key in self._notes:
            box = getattr(ui, 'spinBox_' + str(key))
            label = getattr(ui, 'labelSum_' + str(key))
            value = box.value()
            self._notes[key] = value

            row_sum = key * value
            notes_sum += row_sum

            label.setText(str(row_sum))

        for key in self._coins:
            box = getattr(ui, 'spinBoxCoin_' + str(key))
            label = getattr(ui, 'labelCoinSum_' + str(key))
            value = box.value()
            self._coins[key] = value

            row_sum = key * value * 0.01
            coins_sum += row_sum

            label.setText(str(row_sum))

        self.total_sum = notes_sum + coins_sum

        ui.buttonOk.setDisabled(not self.total_sum > 0)
        ui.labelSumTotal.setText(u'{0:.2f}'.format(notes_sum))
        ui.labelCoinSumTotal.setText(u'{0:.2f}'.format(coins_sum))
        ui.labelTotal.setText(u'{:.2f}'.format(self.total_sum))
        self.setWindowTitle(unicode(self.document))

    def save(self):
        document = self.document

        document.comment = res.getLabelText(self.ui.lineEditComment).strip()
        document.notes = ','.join(self.get_notes())
        document.coins = ','.join(self.get_coins())
        document.total_sum = self.total_sum
        document.cash = settings.cash
        document.user = settings.user
        document.save()

        self.parent.historyUpdate.emit()


class DocumentCashOut(DocumentCash):

    model = models.DocumentCashOut

    def __init__(self, parent, *args, **kwargs):
        super(DocumentCashOut, self).__init__(parent, *args, **kwargs)
        self.document = kwargs.get('model', self.model(cash=settings.cash, user=settings.user))

        self.ui.buttonPrint.clicked.connect(self.to_print)

        self.load()
        self.update()

    def to_print(self):
        """Печать отчета"""
        doc = self.document

        p = settings.printer

        p.reset()
        p.set(height=2)
        p.text_center(settings.shop_name).endl()
        p.set()
        p.text_center(settings.shop_address).endl()
        p.text_center(doc.get_name()).endl()
        p.text_center(u'{0} Кассир №{1}'.format(settings.cash.name, settings.user.pk)).endl()
        p.text_center(u' '.join(res.get_datetime(doc.date))).endl()
        p.separator('-')

        for key in sorted(self._notes.iterkeys(), reverse=True):
            value = self._notes[key]
            if not value:
                continue

            p.align(
                left=u'{0} грн x {1} шт = '.format(key, value),
                right=u' {0} ГРН'.format(key * value),
                separator='.'
            ).endl()

        p.separator('-')
        coins_sum = self.get_coins_sum()

        if coins_sum:
            p.align(left=u'Итого купюрами:', right=u'{0} грн'.format(self.get_notes_sum()), separator='.').endl()
            p.endl()
            p.align(left=u'Итого мелочью:', right=u'{0:.2f} грн'.format(self.get_coins_sum()), separator='.').endl()
            p.separator('-')

        p.set(height=2)
        p.text(u'СУММА:')
        p.set(width=2, height=2)
        p.row_length = 11
        p.text_right(u'{0:.2f}'.format(self.total_sum))

        p.set(height=2)
        p.text(u' ГРН')
        p.set()
        p.endl()
        p.comment(doc.get_comment())
        p.feed()
        p.cut()
        p.start()

    def ok_clicked(self):
        self.document.comment = res.getLabelText(self.ui.lineEditComment)

        if not self.document.get_comment():
            self.ui.lineEditComment.setFocus()
            return

        self.save()
        self.to_print()
        self.close()
