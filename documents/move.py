# coding: utf-8

import sys
from PyQt4 import QtCore, QtGui

import res
import models
import documents

from settings import settings
from documents.inventory import DocumentInventoryBase


class DocumentMove(DocumentInventoryBase):

    model = models.DocumentMove

    def __init__(self, parent, model=None, cart=None):
        super(DocumentMove, self).__init__(parent, model)

        self.load()

        self.completer = documents.Completer(self.ui.lineEditComment, self.model, field='comment')

        self.scanner = settings.scanner
        if self.scanner:
            self.scanner.connect(self.onBarcodeScan)

    def closeEvent(self, e):
        super(DocumentMove, self).closeEvent(e)
        if e.isAccepted():
            if self.scanner:
                self.scanner.disconnect(self.onBarcodeScan)

    def onBarcodeScan(self, barcode):
        try:
            product = models.Product.objects.get(barcode=barcode)
        except models.Product.DoesNotExist:
            self.message(u'Товар {0} не найден.'.format(barcode))
            return

        self.addProduct(product)

    def setupUi(self):
        self.ui = res.move.Ui_Dialog()
        ui = self.ui

        ui.setupUi(self)
        ui.buttonPrint.clicked.connect(self.to_print)
        ui.buttonClose.clicked.connect(self.close)
        ui.buttonOk.clicked.connect(self.save)

        res.setColumnsWidth(ui.tableProducts, (30, 100, 100, 400, 100, 70, 70))

        ui.editSearch.returnPressed.connect(self.onSearch)
        ui.buttonSearch.clicked.connect(self.onSearch)

        ui.labelTitle.setText(unicode(self.document))

        ui.editSearch.setFocus()
        self.setModal(False)

    def update(self):
        m = self.document
        self.setWindowTitle(unicode(m))
        self.ui.labelTitle.setText(unicode(m))
        self.ui.labelCash.setText(u'Кассир: {1}    {0} '.format(m.cash, m.user))
        self.ui.lineEditComment.setText(m.get_comment())

    def load(self):
        if not self.document:
            return

        products = models.ProductMove.objects.filter(document=self.document).select_related('product')
        for i in products:
            self.addProduct(i.product, i.count, False)

        self.update()
        self.ui.tableProducts.cellChanged.connect(self.onCellChange)

    def onSearch(self):
        value = self.ui.editSearch.text().trimmed()
        code, count = self.parseSearchQuery(value)

        if not code or not count:
            return

        product = None

        if code in self.products:
            product = self.products[code]['model']
        else:
            try:
                product = models.Product.objects.only('pk', 'article', 'barcode', 'name', 'stock', 'price').get(pk=code)
            except models.Product.DoesNotExist:
                print 'Product not found:', value

        self.ui.editSearch.clear()

        if not product:
            self.message(u'Товар {0} не найден.'.format(value))
            return

        self.addProduct(product, count)

    def to_print(self):
        self.save()

        document = self.document
        p = settings.printer

        p.reset()
        p.set(height=2)
        p.text_center(settings.shop_name).endl()
        p.set()
        p.text_center(settings.shop_address).endl()

        p.text_center(u'{0}'.format(self.document.get_name())).endl()
        p.text_center(u'{0} Кассир №{1}'.format(document.cash.name, document.user.pk)).endl()
        p.text_center(u' '.join(res.get_datetime(document.date))).endl()

        p.comment(document.get_comment()).endl()
        p.separator('-')

        for n, (k, v) in enumerate(self.products.iteritems(), 1):
            p.text(u'{0}. {1} x {count} шт x {model.price} {2}'.format(n, k, settings.currency, **v)).endl()
            p.text(u'  {0}'.format(v['model'])[:p.row_length]).endl().endl()

        p.separator('-')
        p.feed()
        p.cut()
        p.start()

    def save(self):
        comment = res.getLabelText(self.ui.lineEditComment)

        if not comment:
            self.ui.lineEditComment.setFocus()
            return

        if not self.document:
            self.document = self.model(user=settings.user, cash=settings.cash, total_sum=0)

        self.document.comment = comment
        self.document.save()
        self.ui.labelTitle.setText(unicode(self.document))

        for v in filter(lambda x: x['changed'], self.products.itervalues()):
            product, created = models.ProductMove.objects.get_or_create(document=self.document, product=v['model'])
            product.count = v['count']
            product.save()
            v['changed'] = False

        self.parent.journalUpdate.emit()

def main():
    app = QtGui.QApplication(sys.argv)
    settings.update('user', models.User.objects.get(pk=75))
    settings.update('cash', models.Cash.objects.get(pk=2))

    w = DocumentMove(None)
    w.show()
    app.exec_()

if __name__ == '__main__':
    sys.exit(main())
