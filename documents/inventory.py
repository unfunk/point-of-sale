#!/usr/bin/env python
# coding: utf-8


import sys

from collections import OrderedDict
from PyQt4 import QtCore, QtGui

from settings import settings
import models

from django import db
from django.db import transaction

from res import inventory, setColumnsWidth


class DocumentInventoryBase(QtGui.QDialog):

    def __init__(self, parent, model=None):
        super(DocumentInventoryBase, self).__init__(parent)
        self.parent = parent
        self.document = model
        self.products = OrderedDict()

        if not self.document:
            self.document = self.model(
                user=settings.user,
                cash=settings.cash,
                total_sum=0.0
            )

        self.setupUi()

    def closeEvent(self, e):
        if not self.products:
            return

        has_changed = False
        for i in self.products.itervalues():
            if i['changed']:
                has_changed = True
                break

        if not has_changed:
            e.accept()
            return

        reply = QtGui.QMessageBox.question(self,
            u'Сообщение', u'Вы уверены?',
            QtGui.QMessageBox.Yes, QtGui.QMessageBox.No
        )

        if reply == QtGui.QMessageBox.Yes:
            e.accept()
        else:
            e.ignore()

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Escape:
            self.close()

        if e.key() == QtCore.Qt.Key_F1:
            self.ui.editSearch.setFocus()

    def message(self, text):
        QtGui.QMessageBox.about(self, u'Сообщение', text)

    def addRow(self, model, count=1, changed=True):
        table = self.ui.tableProducts

        self.products[model.pk] = dict(
            model=model,
            count=count,
            stock=model.stock,
            changed=changed,
        )

        row_count = table.rowCount()
        table.insertRow(row_count)
        row = table.rowCount() - 1

        font_b = QtGui.QFont('Ubuntu', 12, 700, False)
        font = QtGui.QFont('Ubuntu', 12, 0, False)
        flags = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

        item = QtGui.QTableWidgetItem(str(row + 1))
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        item.setFlags(flags)
        item.setFont(font)
        table.setItem(row, 0, item)

        item = QtGui.QTableWidgetItem(str(model.pk))
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        item.setFont(font_b)
        item.setFlags(flags)
        table.setItem(row, 1, item)

        item = QtGui.QTableWidgetItem(unicode(model.article))
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        item.setFlags(flags)
        item.setFont(font)
        table.setItem(row, 2, item)

        item = QtGui.QTableWidgetItem(unicode(model))
        item.setFlags(flags)
        item.setFont(font)
        item.setData(QtCore.Qt.UserRole, model)
        table.setItem(row, 3, item)

        item = QtGui.QTableWidgetItem(model.get_price())
        item.setTextAlignment(QtCore.Qt.AlignRight)
        item.setFont(font_b)
        item.setFlags(flags)
        table.setItem(row, 4, item)

        item = QtGui.QTableWidgetItem(str(model.stock))
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        item.setFont(font_b)
        item.setFlags(flags)
        table.setItem(row, 5, item)

        item = QtGui.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        item.setFont(font_b)
        table.setItem(row, 6, item)

    def updateRow(self, n, model):
        table = self.ui.tableProducts
        item = table.item(n, 6)
        product = self.products[model.pk]
        item.setText(str(product['count']))
        table.setCurrentItem(item)

    def getRow(self, n):
        table = self.ui.tableProducts

    def getRowByKey(self, key):
        """
        Поиск номера строки в таблице по ключу
        Возвращает номер строки с id равным key
        """
        return self.products.keys().index(key)

    def getKeyByRow(self, row):
        """
        Поиск ключа по номеру строки в таблице
        Возвращает id модели в указанной строке
        """
        return self.products.keys()[row]

    def addProduct(self, model, count=1, changed=True):
        """
        добавляет товар в таблицу или при наличии, увеличивает количество
        """
        key = model.pk
        if not key in self.products:
            self.products[key] = model
            self.addRow(model, count, changed)
        else:
            self.products[key]['count'] += count

        self.updateRow(self.getRowByKey(key), model)

    def onCellChange(self, row, column):
        if column != 6:
            return

        table = self.ui.tableProducts
        item = table.item(row, column)
        value, ok = item.text().toInt()
        product = self.products[self.getKeyByRow(row)]

        if not ok:
            item.setText(str(product['count']))
            return

        product.update(count=value, changed=True)

        color = '#C7CDD7' if product['count'] != product['stock'] else '#FFFFFF'
        for i in range(table.columnCount()):
            table.item(row, i).setBackgroundColor(QtGui.QColor(color))

    def parseSearchQuery(self, value):
        if not value:
            return

        count = 1

        if '+' in value:
            value, count = value.split('+', 1)

        code, ok = value.toULongLong()

        if ok and len(value) == 13:  # ean13 barcode length
            code = int(value[5:-3])

        return code, int(count)


class DocumentInventory(DocumentInventoryBase):

    model = models.DocumentInventory

    def __init__(self, parent=None, model=None):
        super(DocumentInventory, self).__init__(parent, model)
        self.database = {}

        for p in models.Product.objects.only('pk', 'article', 'barcode', 'name', 'stock', 'price'):
            self.database[p.pk] = p

        self.load()

    def setupUi(self):
        ui = inventory.Ui_Dialog()
        self.ui = ui
        self.ui.setupUi(self)

        setColumnsWidth(ui.tableProducts, (50, 70, 100, 500, 80, 90, 90))

        # ui.tableProducts.hideColumn(5)

        ui.buttonClose.clicked.connect(self.close)
        ui.buttonOk.clicked.connect(self.save)

        ui.editSearch.returnPressed.connect(self.onSearch)
        ui.buttonSearch.clicked.connect(self.onSearch)

        ui.editSearch.setFocus()

        if settings.fullscreen:
            self.showMaximized()

    def load(self):
        if not self.document:
            return

        products = models.ProductInventory.objects.filter(document=self.document).select_related('product')

        for i in products:
            self.addProduct(i.product, i.count)

        self.ui.tableProducts.cellChanged.connect(self.onCellChange)

    def save(self):
        # dump data to csv
        '''
        with open('{:%Y-%m-%d_%H%M%S}.csv'.format(datetime.datetime.today()), 'wb') as csvfile:
            writer = csv.writer(csvfile)
            for k, v in self.products.iteritems():
                writer.writerow([k, v.get('count')])
        '''

        # reconnect to database
        db.close_old_connections()

        try:
            with transaction.atomic():
                self.document.save()
                self.ui.labelTitle.setText(unicode(self.document))

                for v in filter(lambda x: x['changed'], self.products.itervalues()):
                    product, created = models.ProductInventory.objects.get_or_create(
                        product=v['model'],
                        document=self.document
                    )
                    product.stock = v['stock']
                    product.count = v['count']
                    v['changed'] = False

                    product.save()
        except Exception, e:
            print e
            self.message(unicode(e))

    def onSearch(self):
        value = self.ui.editSearch.text().trimmed()
        code, count = self.parseSearchQuery(value)

        product = None

        if code in self.products:
            product = self.products[code]['model']
        else:
            #try:
            #    product = models.Product.objects.only('pk', 'article', 'barcode', 'name', 'stock', 'price').get(pk=code)
            #except models.Product.DoesNotExist:
            #    print 'Product not found:', value
            if code in self.database:
                product = self.database.get(code)
            else:
                self.message(u'Товар {} не найден'.format(code))
                return

        self.ui.editSearch.clear()

        #if not product:
            #self.message(u'Товар {0} не найден.'.format(value))
            #return

        self.addProduct(product, count)


def main():
    app = QtGui.QApplication(sys.argv)

    w = DocumentInventory()
    w.show()
    app.exec_()

if __name__ == '__main__':
    sys.exit(main())
