# -*- coding: utf-8 -*-

from collections import namedtuple

from PyQt4 import QtCore, QtGui
from django.db import transaction

from settings import settings
import documents
import models
import res


class DocumentInvoice(QtGui.QDialog):

    model = models.DocumentInvoice
    products = []

    def __init__(self, parent, *args, **kwargs):
        super(DocumentInvoice, self).__init__(parent)

        self.parent = parent
        self.cart = kwargs.get('cart')

        self.ui = res.invoice.Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.buttonClose.clicked.connect(self.close)
        self.ui.buttonOk.clicked.connect(self.save)
        self.ui.buttonPrint.clicked.connect(self.to_print)

        if kwargs.get('model'):
            self.ui.buttonOk.setDisabled(True)
            self.ui.lineEditComment.setReadOnly(True)
        else:
            self.ui.buttonPrint.setDisabled(True)

        self.document = kwargs.get('model') or models.DocumentInvoice(
            user=settings.user,
            cash=settings.cash,
            total_sum=self.cart.total,
            customer=settings.user
        )

        self.load(self.document, self.cart)
        self.update()

        self.completer = documents.Completer(self.ui.lineEditComment, models.User, field='name')
        self.ui.lineEditComment.setFocus()

    def save(self):
        comment = res.getLabelText(self.ui.lineEditComment)

        if not comment:
            self.ui.lineEditComment.setFocus()
            return

        with transaction.atomic():
            self.document.comment = comment
            self.document.save()

            self.close()
            self.parent.clear()

            products = []
            for item in self.products:
                product = models.ProductInvoice(
                    document=self.document,
                    product=item.model,
                    price=item.price,
                    count=item.count,
                )
                products.append(product)
                model = item.model
                model.stock -= item.count
                model.save(update_fields=['stock'])

            models.ProductInvoice.objects.bulk_create(products)

        self.to_print()

    def load(self, document, cart):
        Product = namedtuple('Product', 'id pk model count price')

        items = cart.itervalues() if cart else models.ProductInvoice.objects.filter(document=document).select_related()
        products = []
        for item in items:
            products.append(Product(
                id=item.product.pk,
                pk=item.product.pk,
                model=item.product,
                count=item.count,
                price=item.price
            ))
        self.products = products

    def update(self):
        document = self.document
        products = self.products

        self.setWindowTitle(unicode(document))
        self.ui.labelTitle.setText(unicode(document))
        self.ui.labelCash.setText(u'Кассир: {1}    {0} '.format(document.cash, document.user))
        self.ui.labelTotal.setText(u'Итого: {0:.2f}'.format(document.total_sum))
        self.ui.tableProducts.horizontalHeader().setResizeMode(3, QtGui.QHeaderView.Stretch)
        self.ui.lineEditComment.setText(document.get_comment())

        font = QtGui.QFont('Ubuntu', 12, 0, False)
        font_b = QtGui.QFont('Ubuntu', 12, 700, False)
        flags = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        self.ui.tableProducts.setRowCount(len(products))

        for n, product in enumerate(products):
            model = product.model

            item_n = QtGui.QTableWidgetItem(str(n + 1))
            item_n.setTextAlignment(QtCore.Qt.AlignCenter)
            item_n.setFlags(flags)
            item_n.setFont(font)

            item_id = QtGui.QTableWidgetItem(str(product.pk))
            item_id.setTextAlignment(QtCore.Qt.AlignCenter)
            item_id.setFont(font_b)
            item_id.setFlags(flags)

            item_article = QtGui.QTableWidgetItem(unicode(product.model.article))
            item_article.setTextAlignment(QtCore.Qt.AlignCenter)
            item_article.setFlags(flags)
            item_article.setFont(font)

            item_name = QtGui.QTableWidgetItem(unicode(product.model))
            item_name.setFlags(flags)
            item_name.setFont(font)
            item_name.setData(QtCore.Qt.UserRole, model)

            item_count = QtGui.QTableWidgetItem(u'{:.0f}'.format(product.count))
            item_count.setTextAlignment(QtCore.Qt.AlignCenter)
            item_count.setFont(font_b)
            item_count.setFlags(flags)

            item_price = QtGui.QTableWidgetItem(model.get_price())
            item_price.setTextAlignment(QtCore.Qt.AlignRight)
            item_price.setFont(font_b)
            item_price.setFlags(flags)

            res.set_row_items(self.ui.tableProducts, n,
                item_n,
                item_id,
                item_article,
                item_name,
                item_count,
                item_price
            )

    def to_print(self):
        p = settings.printer
        if not p:
            return

        document = self.document

        p.reset()
        p.set(height=2)
        p.text_center(settings.shop_name).endl()
        p.set()
        p.text_center(settings.shop_address).endl()

        p.text_center(document.get_name()).endl()
        p.text_center(u'{0} Кассир №{1}'.format(document.cash.name, document.user.pk)).endl()
        p.text_center(u' '.join(res.get_datetime(document.date))).endl()

        p.separator('-')
        products = models.ProductInvoice.objects.filter(document=document)
        for n, v in enumerate(products, 1):
            p.text(u'{0}. {1} x {2} шт x {3} {4}'.format(n, v.product.id, v.count, v.price, settings.currency)).endl()
            p.text(u'  {0}'.format(v.product.name)).endl().endl()

        p.set(height=2)
        p.align(left=u'СУММА:', right=u'{0:.2f}'.format(document.total_sum)).endl()
        p.reset()
        p.comment(document.get_comment())
        p.feed()
        p.cut()
        p.start()
