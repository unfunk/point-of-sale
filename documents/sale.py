# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui

import res
import models
from settings import settings

import cart


class DocumentSale(QtGui.QDialog):

    model = models.DocumentSale

    def __init__(self, parent, model=None):
        super(DocumentSale, self).__init__(parent)
        ui = res.sale.Ui_Dialog()
        ui.setupUi(self)
        self.parent = parent

        ui.buttonPrint.clicked.connect(self.print_clicked)
        ui.buttonClose.clicked.connect(self.close)
        ui.buttonOk.clicked.connect(self.save)

        res.setColumnsWidth(ui.tableProducts, (30, 70, 100, 400, 70, 70, 100))

        self.ui = ui
        self.document = model
        self.load()

    def update_labels(self):
        m = self.document
        self.setWindowTitle(unicode(m))
        self.ui.labelTitle.setText(unicode(m))
        self.ui.labelCash.setText(u'Кассир: {1} - {0} '.format(m.cash, m.user))
        self.ui.labelTotal.setText(u'Итого: {0}'.format(m.total_sum))

    def load(self):
        table = self.ui.tableProducts
        doc = self.document

        if not self.document:
            return

        self.update_labels()

        [self.ui.radioButtonCash, self.ui.radioButtonCard][doc.cash.cashless].setChecked(True)

        if not settings.card_terminal:
            self.ui.radioButtonCard.setDisabled(doc.cash.cashless)

        if doc.pk and not doc.is_active:
            self.ui.buttonOk.setDisabled(True)

        products = models.ProductSale.objects.select_related('product').filter(document=doc)
        for n, i in enumerate(products):
            table.insertRow(n)

            item = QtGui.QTableWidgetItem(unicode(n+1))
            item.setData(QtCore.Qt.UserRole, i.product)

            res.set_row_items(
                table,
                n,
                item,
                unicode(i.product.pk),
                unicode(i.product.article),
                unicode(i.product),
                u'{:.0f}'.format(i.count),
                unicode(i.discount) if i.discount else '',
                u'{:.2f}'.format(i.cost)
            )

    @staticmethod
    def to_print(document, document_cart):
        p = settings.printer

        p.reset()
        p.set(height=2)
        p.text_center(settings.shop_name).endl()
        p.set()
        p.text_center(settings.shop_address).endl()
        p.text_center(u'тел. ' + settings.shop_phone).endl()
        p.text_center(u'интернет-магазин:').endl().text_center(settings.shop_url).endl()
        
        p.separator('-')

        p.text_center(u'Чек #{0:05}'.format(document.pk)).endl()
        p.text_center(u'{0} Кассир #{1}'.format(document.cash.name, document.user.pk)).endl()

        p.text_center(u' '.join(res.get_datetime(document.date))).endl()
        p.separator('-')

        for n, i in enumerate(document_cart.itervalues(), 1):
            p.text(u'{n}#{id} {name} {article}'.format(
                n=n,
                id=i.id,
                name=i.name,
                article='' if not i.article else u'арт. '+ i.article
            )).endl()

            p.text_right(u'{price:.2f} x {count} шт{discount} = {sum:.2f}'.format(
                price=i.price,
                count=i.count,
                sum=i.total_sum,
                discount='' if not i.discount else u' - СКИДКА {0}%'.format(i.discount),
            )).endl()
            p.separator('-')

        p.set(height=2)
        p.align(left=u'СУММА:', right=u'{0:.2f}'.format(document_cart.total)).endl()
        p.set()

        if isinstance(document, models.DocumentSale) and document.cash.cashless:
            p.set(height=2)
            p.text_center(u'ОПЛАТА КАРТОЙ')
            p.set()
        else:
            p.align(left=u'НАЛИЧНЫЕ:', right=u'{0:.2f}'.format(document_cart.cash)).endl()
            p.align(left=u'СДАЧА:', right=u'{0:.2f}'.format(document_cart.change)).endl()

        p.separator('-')
        p.text_center(u'СПАСИБО ЗА ПОКУПКУ!').endl()
        p.set()
        p.feed()
        p.cut()
        p.start()

    def print_clicked(self):
        document_cart = cart.Cart()
        document_cart.fill(self.document)
        self.to_print(self.document, document_cart)

    def save(self):
        is_card = self.ui.radioButtonCard.isChecked() and bool(settings.card_terminal)
        self.document.cash = settings.card_terminal if is_card else settings.cash
        self.document.save(update_fields=['cash'])
        self.close()

        self.parent.historyUpdate.emit()

