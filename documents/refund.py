# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui
from django.db import transaction

import res
import models
from settings import settings

class DocumentRefund(QtGui.QDialog):

    model = models.DocumentRefund
    is_new_document = False
    document = None

    def __init__(self, parent, model=None, base_document=None):
        super(DocumentRefund, self).__init__(parent)

        ui = res.refund.Ui_Dialog()
        ui.setupUi(self)
        self.parent = parent
        self.ui = ui

        self.is_new_document = not model

        ui.buttonClose.clicked.connect(self.close)
        ui.buttonOk.clicked.connect(self.ok_clicked)
        ui.buttonPrint.clicked.connect(self.to_print)
        ui.lineEditComment.editingFinished.connect(self.comment_edited)

        res.setColumnsWidth(ui.tableProducts, (30, 70, 120, 400, 70, 70))

        if self.is_new_document:
            self.document = self.create(base_document)
        else:
            self.document = self.load(model)
            ui.checkBoxSelectAll.hide()
            ui.checkBoxDeselectAll.hide()

        self.update_labels()
        self.update_sum()

    def load(self, model):
        table = self.ui.tableProducts
        table.clearContents()
        table.setRowCount(0)

        for row, i in enumerate(model.products()):
            table.insertRow(row)
            item = QtGui.QTableWidgetItem(unicode(row+1))
            item.setData(QtCore.Qt.UserRole, i)

            res.set_row_items(table, row,
                item,
                unicode(i.product.pk),
                unicode(i.product.article),
                unicode(i.product),
                u'{:.0f}'.format(i.count),
                unicode(i.cost)
            )

        return model

    def create(self, document_sale):
        model = self.model(cash=settings.cash, user=settings.user)
        model.document = document_sale

        self.ui.tableProducts.itemDoubleClicked.connect(self.item_double_clicked)
        self.ui.tableProducts.itemClicked.connect(self.item_clicked)
        self.ui.checkBoxSelectAll.clicked.connect(self.select_all_changed)
        self.ui.checkBoxDeselectAll.clicked.connect(self.deselect_all_changed)

        table = self.ui.tableProducts
        table.clearContents()
        table.setRowCount(0)

        row = 0
        for i in model.document.products():
            for n in range(int(i.count)):
                table.insertRow(row)
                item = QtGui.QTableWidgetItem(unicode(row+1))
                item.setData(QtCore.Qt.UserRole, i)
                item.setCheckState(QtCore.Qt.Unchecked)

                res.set_row_items(table, row,
                    item,
                    unicode(i.product.pk),
                    unicode(i.product.article),
                    unicode(i.product),
                    u'1',
                    unicode(i.cost)
                )

                row += 1

        return model

    def update_labels(self):
        self.setWindowTitle(unicode(self.document))
        self.ui.buttonOk.setDisabled(not self.is_new_document)
        self.ui.lineEditComment.setReadOnly(not self.is_new_document)
        self.ui.labelTitle.setText(u'{0}, {0.user}, {0.cash}'.format(self.document))
        self.ui.labelDocument.setText(u'{0}, {0.user}, {0.cash}'.format(self.document.document))
        self.ui.lineEditComment.setText(self.document.get_comment())

        [self.ui.radioButtonCash, self.ui.radioButtonCard][self.document.cash.cashless].setChecked(True)

    def update_sum(self):
        total = 0
        count = 0
        items = self.get_checked_items()
        for i in items:
            c = 1 if self.is_new_document else i.count
            count += c
            total += i.cost * c

        self.ui.labelTotal.setText(u'{:.0f} шт, итого: {:.2f}'.format(
            count,
            total if self.is_new_document else self.document.total_sum
        ))
        self.ui.buttonOk.setDisabled(count == 0 or not self.is_new_document)

        return total

    def get_checked_items(self):
        table = self.ui.tableProducts
        items = []

        for n in range(table.rowCount()):
            item = table.item(n, 0)
            if (item.checkState() == QtCore.Qt.Checked) or not self.is_new_document:
                items.append(item.data(QtCore.Qt.UserRole).toPyObject())

        return items

    def comment_edited(self):
        self.document.comment = res.getLabelText(self.ui.lineEditComment)

    def item_clicked(self, item):
        if item.column() != 0:
            return

        self.update_sum()

    def item_double_clicked(self, item):
        item = self.ui.tableProducts.item(item.row(), 0)
        state = [QtCore.Qt.Checked, QtCore.Qt.Unchecked][item.checkState() == QtCore.Qt.Checked]
        item.setCheckState(state)
        self.update_sum()

    def select_all_changed(self, value):
        self.ui.checkBoxSelectAll.setCheckState(QtCore.Qt.Checked)

        for n in range(self.ui.tableProducts.rowCount()):
            self.ui.tableProducts.item(n, 0).setCheckState(QtCore.Qt.Checked)

        self.update_sum()

    def deselect_all_changed(self, value):
        self.ui.checkBoxDeselectAll.setCheckState(QtCore.Qt.Unchecked)

        for n in range(self.ui.tableProducts.rowCount()):
            self.ui.tableProducts.item(n, 0).setCheckState(QtCore.Qt.Unchecked)

        self.update_sum()

    def ok_clicked(self):
        self.save()
        self.to_print()
        self.close()

    @transaction.atomic
    def save(self):
        items = self.get_checked_items()

        if not items:
            return

        self.document.total_sum = self.update_sum()
        self.document.save()

        products = {}

        for i in items:
            product = products.get(i.product.id)
            if product:
                product.count += 1
            else:
                product = self.document.create_item(i.product, i.cost, 1)
            products[i.product.id] = product

        models.ProductRefund.objects.bulk_create(products.values())

        self.parent.historyUpdate.emit()

    def to_print(self):
        p = settings.printer
        if not p:
            return

        document = self.document

        p.reset()
        p.set(height=2)
        p.text_center(settings.shop_name).endl()
        p.set()
        p.text_center(settings.shop_address).endl()

        p.text_center(document.get_name()).endl()
        p.text_center(u'{0} Кассир №{1}'.format(document.cash.name, document.user.id)).endl()
        p.text_center(u' '.join(res.get_datetime(document.date))).endl()
        p.text_center(u'на основании {}'.format(document.document.get_name())).endl()
        p.text_center(u'{0} Кассир №{1}'.format(document.document.cash.name, document.document.user.id)).endl()
        p.text_center(u' '.join(res.get_datetime(document.document.date))).endl()

        p.separator('-')
        products = document.products()
        for n, v in enumerate(products, 1):
            p.text(u'{0}#{1} x {2:.0f} шт x {3}'.format(n, v.product.id, v.count, v.cost)).endl()
            p.text(u'  {0}'.format(v.product.name)).endl().endl()

        p.separator('-')
        p.set(height=2)
        p.align(left=u'СУММА:', right=u'{0:.2f}'.format(document.total_sum)).endl()
        p.reset()
        p.comment(document.get_comment())
        p.feed(3)
        p.cut()
        p.start()
