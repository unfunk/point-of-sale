# -*- coding: utf-8 -*-

import datetime

from PyQt4 import QtCore

from settings import settings
import models

import documents


class DocumentCashIn(documents.DocumentCash):

    model = models.DocumentCashIn

    user = None
    cash = None
    document = None
    previous = None

    def __init__(self, parent, *args, **kwargs):
        super(DocumentCashIn, self).__init__(parent)

        self.cash = settings.cash
        self.user = settings.user
        self.document = kwargs.get('model', self.model(cash=self.cash, user=self.user))

        self.load()
        self.update()

        self.ui.buttonPrint.setDisabled(True)

        if self.is_first():
            doc = self.get_previous()
            if doc:
                self.ui.labelPreviousSum.setText(u'{:.2f}'.format(doc.total_sum))
                self.ui.labelPreviousDocument.setText(u'{} {}, {}'.format(doc.get_name(), doc.get_time(), doc.get_comment()))
                self.ui.groupBoxPrevious.setVisible(True)
                self.ui.buttonPrevious.clicked.connect(lambda: self.fill_by_previous(doc))

        self.completer = documents.Completer(self.ui.lineEditComment, self.model, field='comment')

    def fill_by_previous(self, doc):
        self.load(doc)
        self.ui.groupBoxPrevious.setVisible(False)
        self.ui.lineEditComment.setFocus()

    def is_first(self):
        return not self.model.objects.filter(
            cash=self.cash,
            date__gte=self.cash.date,
            date__lt=datetime.datetime.now()
        ).exists()

    def get_previous(self):
        today = self.cash.date
        #yesterday = today - datetime.timedelta(days=1)

        return models.DocumentCashOut.objects.filter(
            cash=self.cash,
            date__lt=today,
        ).order_by('date').last()

    def ok_clicked(self):
        self.save()
        self.close()
