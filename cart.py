# -*- coding: utf-8 -*-

import math
import collections

import models


class CartItem:

    def __init__(self, cart, product, count, discount=0.0):
        self._cart = cart
        self.id = product.pk
        self.name = unicode(product).strip()
        self.article = unicode(product.article).strip()
        self.price = product.price
        self.count = count
        self.discount = discount
        self.product = product

    # Код товара
    id = 0

    # Название товара
    name = u''

    # Артикул товара
    article = u''

    # Цена
    price = 0.0

    # Количество
    count = 0

    # Товар
    product = None

    # Процент скидки
    discount = 0.0

    # Сумма
    sum = 0.0

    # Итоговая сумма со скидкой
    total_sum = 0.0

    # Цена со скидкой
    discount_price = 0.0

    def __unicode__(self):
        return u'{id} {name} {article}'.format(
            id=self.id,
            name=self.name,
            article='' if not self.article else u'арт. ' + self.article,
        )


class Cart(collections.OrderedDict):

    # Сумма без скидки
    sum = 0.0

    # Окончательная округленная сумма со скидкой
    total = 0.0

    # Сумма сдачи
    change = 0.0

    # Сумма наличных
    _cash = 0.0

    # Общее количество единиц товаров
    _count = 0

    _precise = 2

    def length(self):
        return self.__len__()

    def clear(self):
        for i in self.items():
            del self[i[0]]
        self.sum = 0
        self.total = 0
        self.change = 0
        self._discount = 0
        self._cash = 0
        self._count = 0

    def fill(self, model):
        if model:
            products = models.ProductSale.objects.filter(document=model)

            for p in products:
                self.add(p.product, p.count, p.discount)

            self.cash = model.cash_sum

    def is_empty(self):
        return not self.__len__()

    def add(self, product, count, discount=0.0):
        """Добавить товар в корзину"""
        id = product.pk
        if id in self:
            if self[id].count + count > 0:
                self[id].count += count
        else:
            self[id] = CartItem(self, product, count, discount)

        self.calc()

    def remove(self, id):
        del self[id]
        self.calc()

    def calc(self):
        sum_count = 0
        sum_price = 0.0
        sum_total = 0.0

        for k, item in self.items():
            item.sum = round(item.count * item.price, self._precise)
            item.discount_price = self.discount_price(item.price, item.discount)
            item.total_sum = self.round_price(item.discount_price * item.count)

            sum_count += item.count
            sum_price += item.sum
            sum_total += item.total_sum

        self.sum = sum_price
        self._count = sum_count
        self.total = round(sum_total, self._precise)
        self.change = self.cash - self.total

    def get_cash(self):
        return self._cash

    def set_cash(self, value):
        self._cash = round(value, self._precise)
        self.change = value - self.total

    cash = property(get_cash, set_cash)

    def discount_price(self, price, discount):
        "Вычисляет сумму с текущей скидкой"
        return price - (price * discount * 0.01)

    def round_price(self, price):
        "Округление цены"
        return math.ceil(round(price, 3) * 20) / 20

    def name(self):
        return self.__unicode__()

    def __unicode__(self):
        return u'Чек ({0}шт) {1}'.format(self.length(), self.total)

    def __repr__(self):
        return u'Cart <Items:{} Cash:{} Change:{} Total sum:{}>'.format(
            len(self), self.cash, self.change, self.total)
