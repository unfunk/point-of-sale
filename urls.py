from django.conf.urls import patterns, include, url

import views

from views import *
from rpc import RpcHandler

urlpatterns = patterns('',
    url(r'^product/(?P<slug>\d{13})/$', ProductView.as_view(slug_field='barcode')),
    url(r'^product/(?P<pk>\d+)/$', ProductView.as_view(), name='product'),
    url(r'^product/(?P<pk>\d+)/stock/$', ProductStockView.as_view()),
    url(r'^cash/$', CashListView.as_view()),
    url(r'^cash/(?P<pk>\d+)/$', CashView.as_view()),
    url(r'^cash/(?P<pk>\d+)/expenses/', CashExpensesView.as_view()),
    url(r'^inventory/load/$', views.InventoryLoadView.as_view()),
    url(r'^inventory/load/(?P<pk>\d+)/$', views.InventoryLoadView.as_view()),
    url(r'^inventory/$', views.InventoryView.as_view()),
    url(r'^chart/sales/$', views.ChartView.as_view()),
    url(r'^xml_rpc/$', RpcHandler.as_view()),
)
