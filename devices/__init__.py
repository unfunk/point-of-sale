# coding: utf-8

import logging
import importlib
import threading


class ConfigureError(Exception):
    pass


def init_device(device_options, key, settings):
    cls = device_options.get('classname')
    module = None
    device = None

    try:
        module = importlib.import_module('.'.join((__name__, cls.lower())))
        cls = getattr(module, cls.split('.')[-1])
    except ImportError, e:
        msg = 'Import device module "{}" error: {}'.format(cls.lower(), str(e))
        logging.error(msg)
        raise ConfigureError(msg)
    except AttributeError:
        msg = 'Class "{}" not exists in "%s"'.format(cls, module)
        logging.error(msg)
        raise ConfigureError(msg)

    try:
        device = cls(**device_options)
        logging.info('Device {classname}({url}) initialized'.format(**device_options))
    except ConfigureError, e:
        logging.error(unicode(e))

    settings.update(key, device)


def init(settings):
    for key, value in settings.items():
        if type(value) is dict and 'classname' in value:
            init_thread = threading.Thread(target=init_device, args=(value, key, settings))
            init_thread.start()
