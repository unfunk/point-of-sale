# coding: utf-8

import sys
import time
import fcntl
import serial
import logging
import socket
import select

from PyQt4 import QtCore

from devices import ConfigureError


class ScannerWorker(QtCore.QThread):

    protocols = {}
    options = {}
    _callbacks = []
    _barcodeScan = QtCore.pyqtSignal(str, name='barcodeScan')

    def __init__(self, *args, **kwargs):
        super(ScannerWorker, self).__init__()
        self.options = kwargs
        self.prefix = kwargs.get('prefix')
        self.suffix = kwargs.get('suffix')

    def connect(self, callback):
        if self._callbacks:
            self._barcodeScan.disconnect(self._callbacks[-1])

        self._callbacks.append(callback)
        self._barcodeScan.connect(callback)

    def disconnect(self, callback):
        self._callbacks.remove(callback)
        self._barcodeScan.disconnect(callback)
        if self._callbacks:
            self._barcodeScan.connect(self._callbacks[-1])

    def read(self, data):
        left, right = 0, 0
        if data.startswith(self.prefix):
            left = len(self.prefix)

        if data.endswith(self.suffix):
            right = len(data) - len(self.suffix)
        return data[left:right]

    def emit(self, data):
        data = self.read(data)
        if data:
            logging.debug('Scanner: "%s"', repr(data))
            self._barcodeScan.emit(data)

    @classmethod
    def create(cls, *args, **kwargs):
        url = kwargs.get('url')
        if not url:
            raise ConfigureError('Scaner url is not set')

        proto, port = url.split(':', 1)

        if not cls.protocols:
            for k, c in sys.modules[__name__].__dict__.items():
                if type(c) == type(ScannerWorker) and hasattr(c, 'proto'):
                    cls.protocols[getattr(c, 'proto')] = c

        if proto not in cls.protocols:
            raise NotImplementedError('Not implemented protocol ' + proto)

        return cls.protocols.get(proto)(*args, **kwargs)


class SerialScanner(ScannerWorker):

    proto = 'serial'

    port = None

    def __init__(self, *args, **kwargs):
        super(SerialScanner, self).__init__(*args, **kwargs)

        serial_options = (
            'port', 'baudrate', 'bytesize', 'parity',
            'stopbits', 'timeout', 'xonxoff', 'rtscts',
            'writeTimeout', 'dsrdtr', 'interCharTimeout'
        )

        options = {k: kwargs[k] for k in serial_options if k in kwargs}

        try:
            self.port = serial.Serial(**options)
            fcntl.flock(self.port.fileno(), fcntl.LOCK_EX | fcntl.LOCK_NB)
            self.port.read(self.port.inWaiting())
        except (serial.SerialException, IOError) as e:
            logging.error(u'Serial error: {}'.format(str(e).decode('utf-8')))
            #raise ConfigureError(u'Serial error: {}'.format(str(e).decode('utf-8')))

    def run(self):
        if not self.port or not self.port.isOpen():
            raise ConfigureError('Serial port "{}" is not open'.format(self.options.get('port')))

        while True:
            try:
                self.emit(self.port.readline())
            except serial.SerialException, e:
                logging.error('Serial error', e)
                break


class SocketScanner(ScannerWorker):

    proto = 'socket'

    socket = None

    def __init__(self, *args, **kwargs):
        super(SocketScanner, self).__init__(*args, **kwargs)

        try:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        except socket.error, e:
            raise ConfigureError('Failed to create socket. %s', e)

    def run(self):

        host, port = self.options.get('port').split(':')
        self.socket.bind((host, int(port)))
        self.socket.listen(5)
        logging.info('Scanner start listening on %s:%s', host, port)
        client, address = self.socket.accept()
        self.socket.setblocking(0)

        while True:
            ready = select.select([client], [], [], 0.2)
            if ready[0]:
                try:
                    data = client.recv(64)
                    logging.debug('barcode: "%s"', data)
                    self.emit(data)
                except socket.error:
                    logging.error('Lost connection to client')
                    pass
            time.sleep(1)
