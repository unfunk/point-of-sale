# coding: utf-8

from devices.scanner import ScannerWorker


class Scanner(object):

    def __init__(self, *args, **kwargs):
        super(Scanner, self).__init__()

        kwargs['proto'], kwargs['port'] = kwargs.get('url').split(':', 1)

        kwargs['prefix'] = kwargs.get('prefix', '')
        kwargs['suffix'] = kwargs.get('suffix', '\r\n')

        self.worker = ScannerWorker.create(*args, **kwargs)
        self.worker.start()

    def connect(self, callback):
        self.worker.connect(callback)

    def disconnect(self, callback):
        self.worker.disconnect(callback)

