# -*- coding: utf-8 -*-

import logging
import textwrap

from devices import printer

class PPLA(printer.LabelPrinter):

    BAR_CODE3OF9 = 'a'
    BAR_UPCA = 'b'
    BAR_UPCE = 'c'
    BAR_INTERLEAVED2OF5 = 'd'
    BAR_CODE128 = 'e'
    BAR_EAN13 = 'f'
    BAR_EAN8 = 'g'
    BAR_HBIC = 'h'
    BAR_CODA = 'i'
    BAR_I25 = 'j'
    BAR_PLESSEY = 'k'
    BAR_UPC2 = 'm'
    BAR_UPC5 = 'n'
    BAR_CODE93 = 'o'
    BAR_POSTNET = 'p'
    BAR_UCCEAN128 = 'q'
    BAR_EAN128KMART = 'r'
    BAR_EAN128RANDOM = 's'
    BAR_TELEPEN = 't'

    FONT_SMALL = 800
    FONT_BOLD = 807

    def __init__(self, *args, **kwargs):
        super(PPLA, self).__init__(*args, **kwargs)
        self.charset = kwargs.get('charset', 'cp1251')

    def set_charset(self, charset):
        self.charset = charset
        return self

    def end(self):
        return self.write('\x0d')

    def soh(self):
        return self.write('\x01')

    def stx(self):
        return self.write('\x02')

    def esc(self):
        return self.write('\x1b')

    def get_version(self):
        self.stx().write('v')
        return self.read()

    def format_mode(self):
        """
        Enters label formatting state.
        """
        self.stx().write('L').end()
        return self

    def exit_format_mode(self):
        """
        Exits from Label Formatting mode.
        """
        return self.write('X').end()

    def set_pixel_size(self, w=1, h=1):
        """
        Sets width and height pixel size.
        """
        return self.write('D%d%d' % (w, h))

    def set_baud_rate(self, rate):
        """
        Sets baud rate.
        """
        valid_rates = (9600, 600, 2400, 19200, 4800, 38400, 1200, 9600, 57600, 115200)

        if rate in valid_rates:
            self.stx().write('KI8{}'.format(valid_rates.index(rate)))
        return self

    def test_port(self):
        """
        Test RS232 port
        """
        self.write('\x02k')
        return self.read()

    def get_conf(self):
        """
        Inquires system configuration
        """
        self.write('\x02KQ')
        return self.read()

    def clear_memory(self, memory='C'):
        # A - RAM module
        # B - flash memory
        # C - default module
        return self.stx().write('q')

    def clear_flash(self):
        return self.esc().write('@0')

    def set_measure_inches(self):
        """
        Sets measurement to inches
        """
        return self.stx().write('n')

    def set_measure_metric(self):
        """
        Sets measurement to metric
        """
        return self.stx().write('m')

    def set_label_length(self, size):
        """
        Sets label size in inches or or mm. 0600=60mm=6inch
        """
        return self.stx().write('M%04d'.format(size))

    def get_memory_status(self):
        """
        Inquires the graphics/fonts and memory status
        """
        self.write('\x02WF')
        return self.read()

    def set_quantity(self, n=1):
        """
        Sets the quantity of labels to print
        """
        return self.write('Q{:04}'.format(int(n))).end()

    def feed(self):
        """
        Feeds a label.
        """
        return self.stx().write('F')

    def set_rate(self, rate):
        """
        Sets label feed rate.
        """
        rates = {
            1.0: 'A', 1.5: 'B', 2.0: 'C', 2.5: 'D', 3.0: 'E', 3.5: 'F',
            4.0: 'G', 4.5: 'H', 5.0: 'I', 5.5: 'J', 6.0: 'K'
        }
        if rate in rates:
            self.stx().write('S' + rates.get(rate, 'F'))
        return self

    def set_back_feed_length(self, length=220):
        """
        Sets stop position and automatic back-feed for the label stock
        """
        self.stx().write('f{:03}'.format(length)).end()
        return self

    def text(self, text, direction=1, fonttype=9, font=0, y=0, x=0, hscale=0, vscale=0):
        self.write('{0}{1}'.format(direction, fonttype))
        self.write('{0}{1}{2:03}{3:04}{4:04}'.format(hscale, vscale, font, y, x))
        self.write(text.encode(self.charset))
        return self.end()

    def heat(self, value=10):
        """
        The heat value affects the darkness of the image. 0-20
        """
        value = 10 if value > 20 or value < 0 else value
        return self.write('H'+ str(value)).end()

    def line(self, x, y, width, height):
        return self.stx().write('L').write('1X11000{y:04}{x:04}L{width:03}{height:03}'.format(**locals())).end()

    def box(self, xstart, ystart, width, height, thickness, direction=1):
        direction = direction if not direction in (1, 2, 3, 4) else 1
        return self.stx().write('{direction}X11000{ystart:04}{xstart:04}B{width:03}{height:03}{thickness:03}{thickness:03}'.format(**locals())).end()

    def end_label(self):
        self.write('E').end()
        return self

    def barcode(self, code, bartype, direction=1, h='0', v='0', height=0, x=0, y=0, with_text=False):
        """
        Prints barcode.
        direction - print direction. 1, 2, 3 or 4
        bartype - barcode type. The range can be A-T, a-z
        h - represents the width of wide bar.
        v - represents the width of narrow bar.
        height - A 3-digit value that represents the bar code height.
        code - a string of data
        """
        self.write(str(direction))
        self.write([bartype, bartype.upper()][with_text])
        self.write('{0}{1}{2:03}{3:04}{4:04}'.format(h, v, height, y, x))
        self.write(code.encode(self.charset)).end()
        return self

    def reset(self):
        """
        Resets the printer.
        """
        self.write('\x01#')

    def download(self, filename):
        """
        Downloads the graphics file.
        """
        self.stx().write('I')
        self.write('A')  # load to RAM module
        #self.write('A')  # 'A' - 7-bit image file
        self.write('b')  # 8-bit BMP file format
        self.write('picture')  # filename
        self.end()
        img = open(filename)
        self.write(img.read())
        img.close()
        return self

    def download_font(self, filename, id=None):
        """
        Downloads the PCL-font.
        """
        font = open(filename)
        header = font.read(6)
        if not id: id = int(header[3:])
        self.stx().write('XAB')
        self.esc().write('*c{:03}'.format(id))
        data = font.read()
        logging.debug('Downloading font #%s %d, %d bytes', filename, id, len(data))
        self.write(data)
        font.close()

    def download_fonts(self):
        #self.download_font('fonts/ubuntu_7.pcl', 800)
        self.download_font('res/fonts/arial_9.pcl', self.FONT_SMALL)
        self.download_font('res/fonts/ubuntu_8.pcl', 801)
        self.download_font('res/fonts/ubuntu_20b.pcl', 805)
        self.download_font('res/fonts/arial_14bi.pcl', 806)
        self.download_font('res/fonts/arial_7.pcl', 807)
        self.download_font('res/fonts/ubuntu_7b.pcl', 810)

    def text_wrap(self, text, x, y, width, font, height, count):
        for n, line in enumerate(textwrap.wrap(text, width), 1):
            self.text(line, font=font, x=x, y=y)
            y -= height
            if n == count:
                break
        return self

    def print_big_label(self, label):
        """
        Prints 58x30 label.
        """

        #self.set_back_feed_length(250)
        self.set_measure_metric().set_label_length(300)
        self.format_mode().set_pixel_size()

        self.text(label.shop_name, font=self.FONT_SMALL, x=120, y=250)

        #self.text(u'Название:', font=807, x=20, y=215)

        self.text_wrap(label.name, x=20, y=215, width=30, font=800, height=30, count=4)

        #if product['brand']:
        #    self.text(u'Бренд:', font=807, x=20, y=190)
        #    self.text(product['brand'], font=800, x=120, y=185)

        #if product['size']:
        #    self.text(u'Размер:', font=807, x=20, y=160)
        #    self.text(product['size'], font=800, x=120, y=155)

        #if product['country']:
        #    self.text(u'Производитель:', font=807, x=20, y=130)
        #    self.text(product['country'], font=800, x=180, y=130)

        if label.article:
            self.text(u'Артикул:', font=807, x=20, y=90)
            self.text(label.article, font=self.FONT_SMALL, x=110, y=88)

        self.text(u'{}'.format(label.id), direction=4, font=self.FONT_SMALL, x=450, y=10)

        #self.text(u'Цена, {currency}:'.format(**product), direction=4, font=800, x=510, y=10)
        #self.line(500, 0, 150, 285)
        self.text(u'{0:.2f}'.format(label.price), direction=4, font=805, x=580, y=10)

        self.barcode(label.barcode, self.BAR_EAN13, x=50, y=0, height=70)

        self.set_quantity(label.quantity)
        self.end_label()

    def print_small_label(self, label):
        """
        Prints 30x20 label.
        """
        #self.set_back_feed_length(280)
        self.set_measure_metric().set_label_length(200)
        self.format_mode().set_pixel_size().heat(15)

        self.text(label.shop_name, font=807, x=50, y=165)

        self.text_wrap(label.name, x=20, y=140, width=26, font=807, height=20, count=2)

        if label.article:
            self.text(u'Арт: {0}'.format(label.article), font=807, x=20, y=100)

        #self.text(u'{id}'.format(**product), direction=4, font=807, x=300, y=10)
        self.line(20, 60, 110, 22)
        self.text(u'{0}'.format(label.id), font=810, x=40, y=57)

        self.text(u'Цена, {0}:'.format(label.currency).upper(), font=807, x=20, y=80)
        self.text(u'{0:.2f}'.format(label.price), font=806, x=135, y=50)

        self.barcode(label.barcode, self.BAR_EAN13, x=40, y=0, height=55, v='2', h='1')

        self.set_quantity(label.quantity)
        self.end_label()

    def print_size(self, text):
        self.set_measure_metric().set_label_length(300)
        self.format_mode().set_pixel_size()

        self.text(u'{0}'.format(text), font=808, x=50, y=20)

        self.set_quantity(1)
        self.end_label()

    def print_big_label_discount(self, label, quantity=1):
        """
        Prints 58x30 label.
        """

        self.set_measure_metric().set_label_length(300)
        self.format_mode().set_pixel_size().heat(10)

        self.text(label.shop_name, font=self.FONT_SMALL, x=120, y=250)

        #self.text(u'Название:', font=807, x=20, y=215)

        self.text_wrap(label.name, x=20, y=215, width=30, font=self.FONT_SMALL, height=30, count=3)

        #if product['brand']:
        #    self.text(u'Бренд:', font=807, x=20, y=190)
        #    self.text(product['brand'], font=800, x=120, y=185)

        #if product['size']:
        #    self.text(u'Размер:', font=807, x=20, y=160)
        #    self.text(product['size'], font=800, x=120, y=155)

        #if product['country']:
        #    self.text(u'Производитель:', font=807, x=20, y=130)
        #    self.text(product['country'], font=800, x=180, y=130)

        if label.article:
            self.text(u'Артикул:', font=self.FONT_BOLD, x=20, y=125)
            self.text(label.article, font=self.FONT_SMALL, x=110, y=123)

        self.text(u'Код: {0}'.format(label.id), direction=4, font=self.FONT_SMALL, x=520, y=20)

        #self.text(product['price'], direction=4, font=805, x=580, y=10)

        old_price = label.old_price
        new_price = label.price
        discount = (1.0 - (new_price / old_price)) * 100
        self.text(u'Цена грн:', font=self.FONT_SMALL, x=10, y=80)
        self.text(u'Скидка {0:.2f}%'.format(discount), font=self.FONT_SMALL, x=260, y=80)

        #self.line(5, 5, 480, 105)

        self.text(u'{0:.2f}'.format(old_price), font=805, x=5, y=10)
        self.text(u'{0:.2f}'.format(new_price), font=805, x=250, y=10)

        self.line(5, 45, 230, 8)

        self.barcode(label.barcode, self.BAR_EAN13, direction=4, x=580, y=20, height=60, v='2', h='1')

        self.set_quantity(quantity)
        self.end_label()
