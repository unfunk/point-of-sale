# -*- coding: utf-8 -*-
import textwrap

from devices import printer

class EPL(printer.LabelPrinter):

    BAR_EAN13 = 'E30', '4'
    quantity = 1

    def raw(self, value):
        return self.write(value + '\n')

    def new(self):
        return self.raw('N')

    def density(self, value=7):
        value = 7 if value > 15 else value
        return self.raw('D' + str(value))

    def speed(self, value=2):
        value = 2 if value > 6 else value
        return self.raw('S' + str(value))

    def direction(self, value=False):
        return self.raw('Z' + ['T', 'B'][value])

    def set_charset(self, value):
        return self.raw('I' + value)

    def form_length(self, label_length, gap_length):
        """
        Use this command to set the form and gap length
        or black line thickness when using the transmissive
        (gap) sensor, black line sensor, or for setting the
        printer into the continuous media print mode.
        """
        return self.raw('Q{0}, {1}'.format(label_length, gap_length))

    def label_width(self, width):
        """
        Use this command to set the width of the printable area of the media
        """
        return self.raw('q'+ str(width))

    def text(self, text, font, x=0, y=0, direction=0, hscale=1, vscale=1, reverse=False):
        """
        Prints an ASCII text string
        """
        direction = 1 if direction > 3 else direction
        reverse = ['N', 'R'][reverse]
        return self.raw('A{0},{1},{2},{3},{4},{5},{6},"{7}"'.format(x, y, direction, font, hscale, vscale, reverse, text.encode(self.charset)))

    def box(self, xstart, ystart, xend, yend, thickness):
        """
        Use this command to draw a box shape.
        """
        return self.raw('X{0},{1},{2},{4},{5}'.format(xstart, ystart, thickness, xend, yend))

    def line(self, x, y, width, height):
        return self.raw('LE{x},{y},{width},{height}'.format(**locals()))

    def end_label(self):
        """
        Use this command to print the contents of the image buffer.
        """
        return self.raw('P' + str(self.quantity)).flush()

    def end_form(self):
        """
        This command is used to end a form store sequence
        """
        return self.raw('FE')

    def barcode(self, code, bartype, direction=0, h=2, v='0', height=0, x=0, y=0, with_text=False):
        params = [str(x), str(y), str(direction), bartype[0], str(h), '2', str(height), ['N', 'B'][with_text], '"'+ code + '"']

        return self.raw('B' + ','.join(params))

    def download_fonts(self):
        pass

    def set_rate(self, value):
        pass

    def set_quantity(self, value):
        self.quantity = value

    def text_wrap(self, text, x, y, width, font, height, count):
        for n, line in enumerate(textwrap.wrap(text, width), 1):
            self.text(line, font=font, x=x, y=y)
            y += height
            if n == count:
                break
        return self

    def print_small_label(self, label):
        self.set_quantity(label.quantity)
        self.new().label_width(240).form_length(160, 24).density(7).speed(4).direction()
        #self.set_charset('C')
        self.text(label.shop_name, 'a', 30, 0)

        lines = textwrap.wrap(label.name, 28)

        self.text(lines[0], font='a', x=10, y=18)
        if len(lines) > 1:
            self.text(lines[1], font='a', x=10, y=36)

        self.text(u'Цена, {0}:'.format(label.currency), font='a', x=10, y=76)
        self.text(u'{0}'.format(label.price), font='b', x=100, y=66)
        self.text(u'{0}'.format(label.id), font='1', x=20, y=98)
        self.line(0, 96, 90, 16)

        if label.article:
            self.text(u'Арт: {0}'.format(label.article), font='a', x=10, y=56)

        self.barcode(label.barcode, self.BAR_EAN13, x=10, y=112, height=50, h=2, with_text=False)
        self.set_quantity(label.quantity)
        self.end_label()
        
    def print_big_label(self, label):
        """
        Prints 58x30 label.
        """
        self.set_quantity(label.quantity)
        self.new().label_width(448).form_length(230, 25).density(10).speed(4).direction()
        #self.charset('C')
        self.barcode(label.barcode, self.BAR_EAN13, x=0, y=180, h=3, height=40, with_text=False)


        if label.article:
            self.text(u'Артикул:', font='a', x=15, y=135)
            self.text(label.article, font='d', x=95, y=125)

        self.text_wrap(label.name, 15, 30, 30, 'd', 35, 3)

        self.text(label.shop_name, font='d', x=120, y=0)
        self.text(u'{0}'.format(label.id), direction=3, font='a', x=330, y=220)

        self.text(label.price, direction=3, font='c', x=380, y=180)
        self.line(390, 0, 150, 225)
        self.set_quantity(label.quantity)
        self.end_label()
