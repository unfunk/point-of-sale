# coding: utf-8

import os
import sys
import serial
import socket
import multiprocessing
import subprocess

from devices import ConfigureError


class Printer(object):

    def __init__(self, *args, **kwargs):
        self.kwargs = kwargs
        self.port = PrinterPort.create(*args, **kwargs)

    def write(self, data):
        self.port.write(data)
        return self

    def read(self):
        return self.port.read()

    def start(self):
        self.port.start()


class LabelPrinter(Printer):

    class Label(object):

        LABEL_SMALL = 2
        LABEL_BIG = 3
        LABEL_BIG_DISCOUNT = 4

        id = 0
        name = ''
        article = ''
        barcode = ''
        price = 0.0
        category = 0
        full_name = ''
        currency = ''
        shop_name = ''
        quantity = 1
        old_price = 0

        def __init__(self, id, name, article, barcode, price, category=0, full_name='', currency='', shop_name='', quantity=1, old_price=0):
            self.id = id
            self.name = name
            self.article = article
            self.barcode = barcode
            self.price = float(price)
            self.category = category
            self.full_name = full_name
            self.currency = currency
            self.shop_name = shop_name
            self.quantity = int(quantity)
            self.old_price = float(old_price)

    @staticmethod
    def create_label(*args, **kwargs):
        return LabelPrinter.Label(*args, **kwargs)


class PrinterPort(object):

    data = ''
    options = {}
    protocols = {}

    def __init__(self, *args, **kwargs):
        self.kwargs = kwargs
        if not kwargs.get('port'):
            raise ConfigureError('Printer port not set')

    def write(self, data):
        self.data += data

    def clear(self):
        self.data = ''

    def start(self):
        raise NotImplementedError

    @classmethod
    def create(cls, *args, **kwargs):
        url = kwargs.get('url')
        if not url:
            raise ConfigureError('Printer url not set')

        proto, port = url.split(':', 1)

        if not cls.protocols:
            for c in sys.modules[__name__].__dict__.values():
                if type(c) == type and issubclass(PrinterPort, cls) and hasattr(c, 'proto'):
                    cls.protocols[getattr(c, 'proto')] = c

        if proto not in cls.protocols:
            raise NotImplementedError('Not implemented protocol ' + proto)

        kwargs.update(port=port, proto=proto)

        return cls.protocols.get(proto)(*args, **kwargs)


class PrinterPortSerial(PrinterPort):

    proto = 'serial'

    def __init__(self, *args, **kwargs):
        super(PrinterPortSerial, self).__init__(*args, **kwargs)

        if not os.path.exists(kwargs.get('port')):
            raise ConfigureError('Serial port does not exists')

    def start(self):
        serial_options = (
            'port', 'baudrate', 'bytesize', 'parity',
            'stopbits', 'timeout', 'xonxoff', 'rtscts',
            'writeTimeout', 'dsrdtr', 'interCharTimeout'
        )

        options = {k: self.kwargs[k] for k in serial_options if k in self.kwargs}

        port = serial.Serial(**options)
        port.write(self.data)
        self.clear()
        port.close()


class PrinterPortLPR(PrinterPort):

    proto = 'lp'

    def __init__(self, *args, **kwargs):
        super(PrinterPortLPR, self).__init__(*args, **kwargs)

        port = kwargs.get('port')
        queues = self.get_queues()
        if port not in queues:
            raise ConfigureError('Unknown printer "{}". ({})'.format(port, ', '.join(queues)))

    def start(self):
        port = self.kwargs.get('port')
        with open(os.devnull, 'w') as devnull:
            subprocess.call(['cancel', port], stderr=devnull)
        p = subprocess.Popen(['lpr', '-P{}'.format(port), '-o raw'], stdin=subprocess.PIPE)
        p.communicate(self.data)
        p.stdin.close()
        self.clear()

    @staticmethod
    def get_queues():
        queues = []
        try:
            output = subprocess.check_output(['lpstat', '-p'], universal_newlines=True)
        except subprocess.CalledProcessError:
            return []
        for line in output.split('\n'):
            if line.startswith('printer'):
                queues.append(line.split(' ')[1])
        return queues


class PrinterPortFile(PrinterPort):

    proto = 'file'

    thread = None

    def __init__(self, *args, **kwargs):
        super(PrinterPortFile, self).__init__(*args, **kwargs)
        port = kwargs.get('port')

        if not os.path.exists(port):
            raise ConfigureError('Device {} does not exists'.format(port))

        if not os.access(port, os.W_OK):
            raise ConfigureError('Device {} is not writable'.format(port))

    @staticmethod
    def worker(port, data):
        with open(port, 'w') as f:
            f.write(data)
        # sys.exit()

    def start(self):
        if self.thread and self.thread.is_alive():
            self.thread.terminate()

        self.thread = multiprocessing.Process(target=self.worker, args=(self.kwargs.get('port'), self.data))
        self.thread.daemon = True
        self.thread.start()
        self.clear()


class PrinterPortSocket(PrinterPort):

    proto = 'socket'
    address = ()

    def __init__(self, *args, **kwargs):
        super(PrinterPortSocket, self).__init__(*args, **kwargs)

        self.address = self.kwargs.get('port')

        if type(self.address) is not tuple and len(self.address) != 2:
            raise ConfigureError('Invalid host address {}'.format(self.address))

    def start(self):
        handle = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        handle.settimeout(1)
        handle.connect(self.address)
        handle.send(self.data)
        handle.close()
        self.clear()


class PrinterPortTest(PrinterPort):

    proto = 'test'

    def __init__(self, *args, **kwargs):
        super(PrinterPortTest, self).__init__(*args, **kwargs)

    def start(self):
        print self.data.decode(self.kwargs.get('charset'))
        self.clear()

