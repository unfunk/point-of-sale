# -*- coding: utf-8 -*-

from devices import printer


class ESCPOS(printer.Printer):

    def __init__(self, *args, **kwargs):
        super(ESCPOS, self).__init__(*args, **kwargs)

        self.charset = kwargs.get('charset', 'cp866')
        self.row_length = int(kwargs.get('row_length', 32))
        self.feed_length = int(kwargs.get('feed_length', 2))

    def esc(self):
        self.write('\x1b')
        return self

    def endl(self):
        return self.write('\n')

    def reset(self):
        return self.esc().write('\x40')

    def feed(self, n=0):
        if not n:
            n = self.feed_length
        self.esc()
        return self.write('\x64' + chr(min(255, n)))

    def set(self, width=1, height=1):
        mode = 0
        self.row_length = int(self.kwargs.get('row_length'))

        if height > 1:
            mode |= 16

        if width > 1:
            mode |= 32
            self.row_length = int(self.row_length / 2)

        return self.esc().write('!' + chr(mode))

    def bold(self, value=False):
        return self.esc().write('\x45' + ['\x00', '\x01'][value])

    def inverted(self, value=False):
        return self.esc().write('\x42' + ['\x00', '\x01'][value])

    def cut(self, full=True):
        return self.esc().write('\x56' + ['\x01', '\x00'][full])

    def text(self, t):
        return self.write(t.encode(self.charset))

    def text_right(self, t):
        return self.text(u'{0: >{1}}'.format(t, self.row_length))

    def text_center(self, t):
        return self.text(u'{0: ^{1}}'.format(t, self.row_length))

    def align(self, left='', right='', separator=' '):
        return self.text(u'{0:{3}<{2}}{1}'.format(left, right, self.row_length - len(right), separator))

    def separator(self, fill='*'):
        return self.text(u'{0:{0}^{1}}'.format(fill, self.row_length)).endl()

    def comment(self, comment):
        if comment:
            self.endl().align(left=comment, separator='_').endl()
        return self