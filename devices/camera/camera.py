# coding: utf-8

import cv2
import logging
import threading
import multiprocessing

class Camera(object):

    class CameraError(Exception):
        pass

    DEFAULT_FRAME_WIDTH = 640
    DEFAULT_FRAME_HEIGHT = 480

    DEFAULT_PREVIEW_WIDTH = 320
    DEFAULT_PREVIEW_HEIGHT = 240

    options = {}

    exit_event = multiprocessing.Event()

    image = None

    def __init__(self, *args, **kwargs):
        super(Camera, self).__init__()
        kwargs['frame_width'] = kwargs.get('frame_width', self.DEFAULT_FRAME_WIDTH)
        kwargs['frame_height'] = kwargs.get('frame_height', self.DEFAULT_FRAME_HEIGHT)
        kwargs['preview_width'] = kwargs.get('preview_width', self.DEFAULT_PREVIEW_WIDTH)
        kwargs['preview_height'] = kwargs.get('preview_height', self.DEFAULT_PREVIEW_HEIGHT)
        kwargs['callback'] = kwargs.get('callback')
        kwargs['camera_index'] = kwargs.get('camera_index', -1)

        self.options = kwargs

    def stop(self):
        self.exit_event.set()

    def take(self):
        return cv2.cvtColor(self.image, cv2.COLOR_BGR2RGB)

    def save(self, path, params=None):
        logging.debug('Save picture to "{}"'.format(path))
        try:
            return cv2.imwrite(path, self.take(), params)
        except cv2.error:
            raise self.CameraError(u'Cant save picture to "{}"'.format(path))

    def setup(self, **kwargs):
        self.options.update(**kwargs)

    def _capture_process(self, pipe_parent):
        capture = cv2.VideoCapture()

        if not capture.open(self.options['camera_index']):
            pipe_parent.send(None)
            logging.error('Camera open error')

        capture.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, self.options['frame_width'])
        capture.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, self.options['frame_height'])

        logging.debug('Capture starting. Frame size: {}x{}, preview size: {}x{}'.format(
            self.options['frame_width'], self.options['frame_height'],
            self.options['preview_width'], self.options['preview_height'],
        ))

        while not self.exit_event.is_set():
            ok, frame = capture.read()

            if not ok or self.exit_event.is_set():
                break

            pipe_parent.send(frame)

        capture.release()
        logging.debug('Capture stopped')

    @property
    def is_active(self):
        return not self.exit_event.is_set()

    def start(self):
        if not self.options['callback']:
            raise self.CameraError('"callback" option is not set')

        self.exit_event.clear()
        process_frame_callback = self.options['callback']
        preview_size = (self.options['preview_width'], self.options['preview_height'])

        pipe_parent, pipe_child = multiprocessing.Pipe(duplex=False)
        cam_process = threading.Thread(target=self._capture_process, args=(pipe_child, ))
        cam_process.start()

        while not self.exit_event.is_set():
            frame = pipe_parent.recv()

            if frame is None:
                break

            self.image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            process_frame_callback(cv2.resize(self.image, dsize=preview_size))
            cv2.waitKey(1)
