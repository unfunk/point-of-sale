# coding: utf-8

import datetime

from django.db import connections
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.generic import TemplateView, ListView, View
from django.views.generic.list import BaseListView
from django.views.generic.detail import BaseDetailView, DetailView
from django.views.decorators.cache import cache_page, never_cache
from django.forms.models import model_to_dict
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
import cStringIO, csv

from settings import settings
import rpc
import res
import models
import forms


class JSONResponseMixin(object):
    def render_to_response(self, context):
        return JsonResponse(context, safe=False)


class JSONPResponseMixin(JSONResponseMixin):
    def render_to_response(self, context):
        response = super(JSONPResponseMixin, self).render_to_response(context)
        callback = self.request.GET.get('jsonp', 'processJSON')
        response.content = callback + '(' + response.content + ');'
        return response


class CacheMixin(object):
    cache_timeout = 60

    def get_cache_timeout(self):
        return self.cache_timeout

    def dispatch(self, *args, **kwargs):
        return cache_page(self.get_cache_timeout())(super(CacheMixin, self).dispatch)(*args, **kwargs)


class ProductView(DetailView):
    model = models.Product
    form_class = forms.UploadPictureForm
    
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ProductView, self).dispatch(*args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super(ProductView, self).get_context_data(**kwargs)
        context.update(form=self.form_class())
        return context
        
    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            form.save()
        return HttpResponse('OK')


class ProductStockView(BaseDetailView, JSONResponseMixin):
    model = models.Product

    def get_context_data(self, **kwargs):
        pk = self.kwargs.get(self.pk_url_kwarg, None)
        product = models.Product.objects.get(pk=pk)
        return list(product.get_stock())

class CashView(CacheMixin, BaseDetailView, JSONResponseMixin):
    model = models.Cash
    cache_timeout = 5

    def get_context_data(self, **kwargs):
        pk = self.kwargs.get(self.pk_url_kwarg, None)
        obj = rpc.cash_stat(pk)

        return obj

class CashExpensesView(BaseDetailView, JSONResponseMixin):
    model = models.Cash

    def get_context_data(self, **kwargs):
        pk = self.kwargs.get(self.pk_url_kwarg, None)
        obj = rpc.get_expenses(pk)

        return obj

class CashListView(ListView):
    model = models.Cash
    queryset = model.objects.filter(pk__in=[3, 4, 6, 7])


class CashoutListView(CacheMixin, BaseDetailView, JSONResponseMixin):

    def get_queryset(self):
        return rpc.cash_invoice(self.kwargs.get(self.pk_url_kwarg, None))

    def get_context_data(self, **kwargs):
        queryset = self.get_queryset()
        return {'test': 'value'}


class InventoryView(View):

    model = models.DocumentInventory
    fields = ('id', 'name', 'article', 'stock', 'price', 'barcode')

    def get_queryset(self):
        return models.Product.objects.only(*self.fields)

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(InventoryView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        data = ''
        queue = cStringIO.StringIO()
        writer = csv.writer(queue, delimiter=';')
        for product in self.get_queryset():
            writer.writerow([unicode(getattr(product, f)).encode('utf-8') for f in self.fields])

        return HttpResponse(queue.getvalue())

    def post(self, request, *args, **kwargs):
        doc = self.model(user=models.User.objects.get(pk=75), cash=models.Cash.objects.get(pk=2), total_sum=0)
        doc.save()

        products = []
        for pair in request.body.split('&'):
            key, value = pair.split('=')
            product = models.Product.objects.get(pk=key)
            item = doc.create_item(product=product, count=int(value))
            products.append(item)
        models.ProductInventory.objects.bulk_create(products)

        return HttpResponse('ok ' + str(doc.pk))

class ChartView(ListView):
    model = models.Cash
    queryset = model.objects.filter(pk__in=[3, 4, 6, 7])
    template_name = 'shop/chart.html'
    date_fmt = '%d.%m.%Y'

    def get_sales(self, cash, period_start, period_end):
        db = rpc.get_db_override(cash.id)
        return {i['day'].strftime(self.date_fmt): i['total'] for i in cash.sales(db, period_start, period_end)}

    def get_context_data(self, **kwargs):
        context = super(ChartView, self).get_context_data(**kwargs)

        today = datetime.datetime.today()
        month_begin = datetime.datetime(today.year, today.month, 1)

        dates = [i.strftime(self.date_fmt) for i in res.datespan(month_begin, today)]
        sales = {s.id: self.get_sales(s, month_begin, today) for s in self.object_list}
        keys = sales.keys()

        rows = []
        for date in dates:
            row = [date]

            for k in keys:
                row.append(round(sales[k].get(date, 0), 2))

            rows.append(row)

        context.update(
            dates=dates,
            data=rows
        )

        return context


class InventoryLoadView(BaseListView, JSONResponseMixin):
    
    fields = ('id', 'name', 'article', 'stock', 'price', 'barcode')

    def get_queryset(self):
        return models.Product.objects.only(*self.fields)

    def get_context_data(self, **kwargs):
        data = []
        queryset = self.get_queryset()
        
        pk = self.kwargs.get('pk')
        if pk:
            queryset = queryset.filter(pk=pk)
        
        for product in queryset:
            data.append(model_to_dict(product, self.fields))

        return {'products': data, 'count': len(data) }

