# coding: utf-8

from PyQt4 import QtCore


class AsyncTask(QtCore.QThread):

    callback = QtCore.pyqtSignal(object)

    def __init__(self, worker, finish_callback=None, args=(), kwargs=None):
        super(AsyncTask, self).__init__()

        if kwargs is None:
            kwargs = {}

        self.worker = worker
        self.args = args
        self.kwargs = kwargs

        if finish_callback:
            self.callback.connect(finish_callback)

        self.start()

    def run(self):
        result = self.worker(*self.args, **self.kwargs)
        self.callback.emit(result)
