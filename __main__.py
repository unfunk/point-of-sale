#!/usr/bin/env python
# coding: utf-8

import sys
import locale
import logging

from PyQt4 import QtGui

import models
import main
import login
import devices

from settings import settings


def run():
    locale.setlocale(locale.LC_ALL, locale.normalize(settings.locale))
    app = QtGui.QApplication(sys.argv)

    try:
        devices.init(settings)
    except devices.ConfigureError, e:
        logging.error(unicode(e))
        main.MainForm.message(e)

    if settings.place:
        try:
            settings.cash = models.Cash.objects.get(pk=settings.place)
        except models.Cash.DoesNotExist:
            logging.error('Cash #{%s} is not exists', settings.place)
            return
    else:
        logging.warning('Property "place" is not set')
        return

    if settings.card_terminal_id:
        try:
            settings.card_terminal = models.Cash.objects.get(pk=settings.card_terminal_id)
        except models.Cash.DoesNotExist:
            logging.error('Cash terminal #{%s} is not exists', settings.card_terminal_id)
            return
    else:
        settings.card_terminal = None

    user, ok = login.LoginDialog.get_user()
    if not ok:
        return

    settings.user = user

    form = main.MainForm(user)

    app.exec_()


def run_daemon():
    import daemon
    daemon.main()


def run_console():
    import console
    console.main()


def run_inventory():
    import documents.inventory
    documents.inventory.main()


if __name__ == '__main__':
    {
        'pos': run,
        'console': run_console,
        'daemon': run_daemon,
        'inventory': run_inventory,
    }.get(settings.args.mode, run)()

