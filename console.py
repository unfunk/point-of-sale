#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import cmd
import csv
import collections

from settings import settings
import models
import rpc

from django.db import connections, transaction


class ShopConsole(cmd.Cmd):

    def __init__(self):
        cmd.Cmd.__init__(self)
        self.prompt = '> '

    def do_quit(self, line):
        sys.exit(1)

    def do_products(self, line):
        ids = [int(id) for id in line.split()]
        d = models.ProductSale.objects.filter(document__is_active=True)
        if ids:
            d = d.filter(product_id__in=ids)
        for i in d:
            print u'{0}\t{1:.2f}'.format(i.document, i.document.total_sum)

    def do_document(self, line):
        d = models.Document.objects.get(pk=int(line))
        print d
        if d.type == models.Document.DOC_SALE:
            for i in models.ProductSale.objects.filter(document=d):
                print u'{0:05}\t{1}\t{2}%\t{3:.2f}\t{4}'.format(i.product.pk, i.count, i.discount, i.cost, i.product)

    def do_moves(self, line):
        product = models.Product.objects.get(pk=int(line))
        s = 0
        print product.pk, product.article, product
        print
        items = []
        items += models.ProductInvoice.objects.filter(product=product)
        items += models.ProductMove.objects.filter(product=product)
        items += models.ProductInventory.objects.filter(product=product)
        items += models.ProductSale.objects.filter(product=product)
        items += models.ProductRefund.objects.filter(product=product)
        for i in items:
            print u'{0}, {1} {2}'.format(i.document, i.count, i.document.get_comment())
            s += i.count
        print 'Total:', s

    def print_label(self, label_type, data):
        conn = connections['backend']

        printer = settings.label_printer
        currency = settings.currency
        shop_name = settings.shop_name

        labels = {}
        for i in conn.get_products(data.keys()):
            product_id = i['id']
            labels[product_id] = printer.create_label(
                id=product_id,
                name=i['name'],
                article=i['article'],
                barcode=i['barcode'],
                price=i['price'],
                currency=currency,
                shop_name=shop_name,
                quantity=data[product_id],
            )

        label_method = {
            2: printer.print_small_label,
            3: printer.print_big_label,
        }.get(label_type)

        for label in labels.values():
            label_method(label)

        printer.start()

    def parse_items(self, line):
        items = collections.OrderedDict()

        for i in line.split():
            v = i.split('x', 1)

            if len(v) == 1:
                v.append(1)

            items.update({int(v[0]): items.get(v[0], 0) + int(v[1])})

        return items

    def do_printb(self, line):
        self.print_label(3, self.parse_items(line))

    def do_print(self, line):
        self.print_label(2, self.parse_items(line))

    def do_delete(self, line):
        ids = line.split()
        documents = models.Document.objects.filter(pk__in=ids)

        if not documents:
            print 'Documents not found'
            return

        for d in documents:
            print d, '-', d.cash, '-', d.total_sum

        answer = raw_input('Delete selected (' + str(len(documents)) + ') documents? [y/N]: ').lower()
        if answer == 'y':
            for d in documents:
                d.delete()

    def do_loadcsv(self, filename):
        try:
            f = open(filename)
        except IOError, msg:
            print msg
            return

        with transaction.atomic():
            doc = models.DocumentInventory(cash=models.Cash.objects.get(pk=2), user=models.User.objects.get(pk=75), total_sum=0)
            doc.save()

            products = []
            for line in csv.reader(f):
                print 'Product', line[0]
                product = models.Product.objects.get(pk=line[0])
                products.append(doc.create_item(product, int(line[1])))

            models.ProductInventory.objects.bulk_create(products)

            print doc


    def do_EOF(self, line):
        return True

    # shortcuts
    do_q = do_quit


def main():

    console = ShopConsole()

    try:
        #if len(sys.argv) > 1:
        #    console.onecmd(' '.join(sys.argv[1:]))
        #else:
        print 'Welcome to shop console'
        console.cmdloop()
    except KeyboardInterrupt:
        print

if __name__ == '__main__':
    main()
