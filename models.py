#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
from settings import settings
import datetime

from django.db import models, connections, transaction


class BaseModel(models.Model):
    class Meta:
        abstract = True
        app_label = settings.app_label


class Cash(BaseModel):
    id = models.IntegerField(primary_key=True, unique=True)
    name = models.CharField(max_length=100)
    cashless = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name

    @property
    def date(self):
        today = datetime.datetime.today()
        return datetime.datetime(today.year, today.month, today.day)

    def documents(self, date=None, places=None):
        date = date or self.date

        types = (
            Document.DOC_CASH_IN,
            Document.DOC_CASH_OUT,
            Document.DOC_REFUND,
            Document.DOC_SALE,
        )

        objects = Document.objects.using(self._state.db).filter(
            date__year=date.year,
            date__month=date.month,
            date__day=date.day,
            type__in=types,
        ).order_by('-date', '-id').select_related('cash', 'user')

        if not places:
            objects = objects.filter(cash=self)

        if places and type(places) in (list, tuple):
            objects = objects.filter(cash__in=filter(None, places))

        return objects

    def stat(self, types=None, date=None):
        fields = {
            Document.DOC_SALE: 'sum_sale',
            Document.DOC_REFUND: 'sum_refund',
            Document.DOC_CASH_IN: 'sum_in',
            Document.DOC_CASH_OUT: 'sum_out',
        }

        if not types:
            types = fields.keys()

        query = Document.objects.using(self._state.db).filter(
            cash=self.pk,
            date__gte=date or self.date,
            type__in=types
        ).values_list('type').annotate(models.Sum('total_sum'))

        result = dict.fromkeys(fields.values(), 0.0)

        for t, s in query:
            result[fields[t]] = s

        result['sum_total'] = result['sum_in'] + result['sum_sale'] - result['sum_out'] - result['sum_refund']

        return result

    def last_user(self):
        date = self.date
        return User.objects.filter(
            last_cash=self,
            last_login__year=date.year,
            last_login__month=date.month,
            last_login__day=date.day
        ).order_by('-last_login').first()

    def count(self):
        return DocumentSale.objects.using(self._state.db).filter(cash=self.pk, date__gte=self.date).count()

    def expenses(self):
        return DocumentCashOut.objects.using(self._state.db).filter(cash=self, date__gte=self.date)

    def sales(self, using, period_start, period_end):
        qs = DocumentSale.objects.using(using).filter(date__gte=period_start, date__lte=period_end, cash=self)
        return qs.extra({'day': 'date(date)'}).values('day').order_by().annotate(total=models.Sum('total_sum'))


class User(BaseModel):
    name = models.CharField(max_length=100)
    login = models.CharField(max_length=30)
    password = models.CharField(max_length=30)
    last_login = models.DateTimeField(auto_now=True)
    last_cash = models.ForeignKey(Cash)
    phone = models.CharField(max_length=30, null=True)
    role = models.ManyToManyField('Role', through='UserRole')

    def __unicode__(self):
        return self.login


class Brand(BaseModel):
    id = models.IntegerField(primary_key=True, unique=True)
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name


class Category(BaseModel):
    id = models.IntegerField(primary_key=True, unique=True)
    name = models.CharField(max_length=100)
    parent = models.IntegerField()

    def __unicode__(self):
        return self.name

    def full_name(self):
        return '/'.join(map(unicode, self.path()))

    def path(self):
        categories = []

        category = self

        while True:
            categories.append(category)

            if category.parent == 0:
                break

            try:
                category = Category.objects.get(pk=category.parent)
            except Category.DoesNotExist:
                category = Category(pk=category.parent, parent=0, name='Category #{}'.format(category.parent))

        categories.reverse()

        return categories


class Product(BaseModel):
    id = models.IntegerField(primary_key=True, unique=True)
    name = models.CharField(max_length=200)
    article = models.CharField(max_length=24)
    barcode = models.CharField(max_length=20)
    brand = models.ForeignKey(Brand, null=True)
    category = models.ForeignKey(Category)
    price = models.FloatField()
    stock = models.IntegerField()

    def __unicode__(self):
        return self.name

    def get_price(self):
        return u'{:.2f}'.format(self.price)

    def get_image_path(self):
        path = settings.get('images_path', '.')
        image_path = '{0}/{1}.jpg'.format(path, self.pk)
        if not os.path.exists(image_path):
            image_path = path + '/0.jpg'
        return image_path

    def get_image(self):
        img = str(self.pk) + '.jpg'
        directory = settings.images_path
        full_path = os.path.join(directory, img)
        img_path = img if os.path.exists(full_path) else None
        return img_path, full_path, directory

    def get_stock(self, stock_id=None):
        conn = connections['backend']
        return conn.get_product_stock(pk=self.pk, stock_id=stock_id)


class Document(BaseModel):

    types = (
        u'Документ',
        u'Чек',
        u'Ввод средств',
        u'Инкассация',
        u'Возврат',
        u'Ввод остатков денег',
        u'Инвентаризация',
        u'Расходная накладная',
        u'Перемещение',
    )

    DOC_UNKNOWN = 0
    DOC_SALE = 1
    DOC_CASH_IN = 2
    DOC_CASH_OUT = 3
    DOC_REFUND = 4
    DOC_MONEY = 5
    DOC_INVENTORY = 6
    DOC_INVOICE = 7
    DOC_MOVE = 8

    user = models.ForeignKey(User)
    date = models.DateTimeField(auto_now_add=True)
    total_sum = models.FloatField()
    is_active = models.BooleanField(default=True)
    cash = models.ForeignKey(Cash)
    type = models.IntegerField()
    external_id = models.CharField(max_length=10)
    comment = models.CharField(max_length=100, null=True)

    def __init__(self, *args, **kwargs):
        super(Document, self).__init__(*args, **kwargs)
        if not self.type:
            self.type = doc_types.get(type(self))

    def get_name(self):
        return u'{0} №{1:05}'.format(self.types[self.type], self.pk or 0)

    def get_comment(self):
        return u'' if not self.comment else self.comment

    def get_time(self, fmt='%H:%M:%S'):
        return self.date.strftime(fmt)

    def get_date(self, fmt='%d.%m.%Y'):
        return self.date.strftime(fmt)

    def get_type(self):
        pass

#    def __format__(self, spec):
#        # TODO
#        return self.get_name()

    def __unicode__(self):
        return u'{0} от {1:%Y-%m-%d %H:%M}'.format(self.get_name(), self.date or datetime.datetime.now())


class DocumentSale(Document):
    cash_sum = models.FloatField()

    def products(self):
        return ProductSale.objects.filter(document=self)

    def create_item(self, product, price, count, discount, cost):
        return ProductSale(
            document=self,
            product=product,
            price=price,
            count=count,
            discount=discount,
            cost=cost
        )

    @staticmethod
    @transaction.atomic()
    def create(user, cash, cart):
        obj = DocumentSale(user=user, cash=cash, total_sum=cart.total, cash_sum=cart.cash)
        obj.save()

        products = []
        for item in cart.itervalues():
            product = obj.create_item(item.product, item.price, item.count, item.discount, item.discount_price)
            products.append(product)
            model = item.product
            model.stock -= item.count
            model.save(update_fields=['stock'])

        ProductSale.objects.bulk_create(products)

        return obj


class DocumentInventory(Document):

    def create_item(self, product, count):
        return ProductInventory(document=self, product=product, count=count)


class DocumentCashIn(Document):
    notes = models.CommaSeparatedIntegerField(max_length=100)
    coins = models.CommaSeparatedIntegerField(max_length=100)


class DocumentCashOut(Document):
    notes = models.CommaSeparatedIntegerField(max_length=100)
    coins = models.CommaSeparatedIntegerField(max_length=100)


class DocumentRefund(Document):
    document = models.ForeignKey(DocumentSale)

    def products(self):
        return ProductRefund.objects.filter(document=self)

    def create_item(self, product, cost, count):
        return ProductRefund(document=self, product=product, cost=cost, count=count)


class DocumentInvoice(Document):
    customer = models.ForeignKey(User)


class DocumentMove(Document):
    pass


class ProductRefund(BaseModel):
    document = models.ForeignKey(DocumentRefund)
    product = models.ForeignKey(Product)
    cost = models.FloatField()
    count = models.FloatField()

    def __unicode__(self):
        return u'{}, {}'.format(self.product, self.count)


class ProductInventory(BaseModel):
    document = models.ForeignKey(DocumentInventory)
    product = models.ForeignKey(Product)
    count = models.IntegerField(null=True)
    stock = models.IntegerField(null=True)

    def __unicode__(self):
        return u'{}, {}'.format(self.product, self.count)


class ProductSale(BaseModel):
    document = models.ForeignKey(DocumentSale)
    product = models.ForeignKey(Product)
    price = models.FloatField()
    discount = models.FloatField()
    count = models.FloatField()
    cost = models.FloatField()

    def __unicode__(self):
        return u'{}, {}'.format(self.product, self.count)


class ProductInvoice(BaseModel):
    document = models.ForeignKey(DocumentInvoice)
    product = models.ForeignKey(Product)
    price = models.FloatField()
    count = models.FloatField()


class ProductMove(BaseModel):
    document = models.ForeignKey(DocumentMove)
    product = models.ForeignKey(Product)
    count = models.IntegerField(null=True, default=0)


class Role(BaseModel):
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name


class UserRole(BaseModel):
    role = models.ForeignKey(Role)
    user = models.ForeignKey(User)


class SalaryCharge(BaseModel):
    date = models.DateField()
    user = models.ForeignKey(User)
    is_active = models.BooleanField(default=True)

    def __unicode__(self):
        return u'Начисление №{0} за {1}'.format(self.pk, self.date)


doc_types = {
    Document: Document.DOC_UNKNOWN,
    DocumentSale: Document.DOC_SALE,
    DocumentCashIn: Document.DOC_CASH_IN,
    DocumentCashOut: Document.DOC_CASH_OUT,
    DocumentRefund: Document.DOC_REFUND,
    DocumentInventory: Document.DOC_INVENTORY,
    DocumentInvoice: Document.DOC_INVOICE,
    DocumentMove: Document.DOC_MOVE,
}


def create_tables(tables):
    from django.db import connection, transaction
    from django.core.management.color import no_style
    pending_references = {}
    seen_models = set()
    style = no_style()
    for model in tables:
        sql, references = connection.creation.sql_create_model(model, style)
        seen_models.add(model)
        for refto, refs in references.items():
            pending_references.setdefault(refto, []).extend(refs)
            if refto in seen_models:
                sql.extend(
                    connection.creation.sql_for_pending_references(
                        refto,
                        style,
                        pending_references))
        sql.extend(
            connection.creation.sql_for_pending_references(
                model,
                style,
                pending_references))
        cursor = connection.cursor()
        for stmt in sql:
            print stmt
            try:
                cursor.execute(stmt)
            except Exception, e:
                print e
                pass
        print
    transaction.commit_unless_managed()
    for model in tables:
        index_sql = connection.creation.sql_indexes_for_model(model, style)
        if index_sql:
            try:
                for stmt in index_sql:
                    print stmt
                    cursor.execute(stmt)
            except Exception, e:
                transaction.rollback_unless_managed()
            else:
                transaction.commit_unless_managed()

if __name__ == '__main__':
    create_tables(model for model in locals().values() if type(model) == type(BaseModel))
