# -*- coding: utf-8 -*-

import datetime

from PyQt4 import QtCore, QtGui

import res
import models


class JournalTable:

    limit = 200

    def __init__(self, parent):
        self.parent = parent
        self.ui = parent.ui

        res.setColumnsWidth(self.ui.tableJournal, (100, 350, 100, 90, 300))
        self.ui.tableJournal.horizontalHeader().setResizeMode(4, QtGui.QHeaderView.Stretch)
        parent.journalUpdate.connect(self.show)

    def clear(self):
        self.ui.tableJournal.clearContents()
        self.ui.tableJournal.setRowCount(0)

    def show(self):
        self.clear()

        documents = models.Document.objects.filter(
            type__in=[
                models.Document.DOC_MOVE,
                models.Document.DOC_INVENTORY,
                models.Document.DOC_INVOICE,
            ]).order_by('-date', '-id')[:self.limit]

        self.ui.tableJournal.setRowCount(documents.count())

        for n, doc in enumerate(documents):
            item_id = QtGui.QTableWidgetItem(unicode(doc.pk))
            item_id.setTextAlignment(QtCore.Qt.AlignRight)
            item_id.setData(QtCore.Qt.UserRole, doc)

            res.set_row_items(self.ui.tableJournal, n,
                item_id,
                doc.get_name(),
                doc.get_date(),
                doc.get_time(),
                doc.get_comment(),
            )

        self.ui.tableJournal.setCurrentCell(0, 0)

