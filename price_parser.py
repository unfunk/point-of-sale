#!/usr/bin/env python
#coding: utf-8

__author__ = 'victor'

import sys
import csv


def parse_csv(raw_price):

    data = []
    for line in csv.reader(raw_price):
        if not line[0]:
            continue

        data.append(dict(
            article=line[0].split('/')[0].strip(),
            name=line[1],
            count=line[2],
            price=line[3],
        ))
        print data[-1]['article']

    return data


def main():
    if len(sys.argv) < 2:
        print 'Not enough parameters. Usage:', sys.argv[0], 'file.html'
        return

    try:
        f = open(sys.argv[1])
    except IOError, msg:
        print msg
        return

    items = parse_csv(f)
    
    total = 0.0
    for i in items:
        total += int(i['count']) * float(i['price'].replace(',', '.'))
        
    print 'Total sum', total


if __name__ == '__main__': main()
