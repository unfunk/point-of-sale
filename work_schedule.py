# -*- coding: utf-8 -*-

import datetime
import collections

from PyQt4 import QtCore, QtGui

import res
import models


class WorkSchedule:

    model = models.SalaryCharge

    def __init__(self, parent):
        self.parent = parent
        self.table = parent.ui.tableWork

        # disable cell resizing
        self.table.verticalHeader().setResizeMode(QtGui.QHeaderView.Fixed)
        self.table.horizontalHeader().setResizeMode(QtGui.QHeaderView.Fixed)

    def show(self):
        users = models.User.objects.filter(role=7).order_by('name')
        user_count = users.count()

        data = self.model.objects.filter(date__lte=self.today(), date__gte=self.from_date()).select_related('user')

        self.data = collections.defaultdict(list)
        for i in data:
            self.data[i.user].append(i.date)

        today = self.today()
        day = today.day
        self.table.setColumnCount(day)
        self.table.setRowCount(user_count)

        for n, user in enumerate(users):
            item = QtGui.QTableWidgetItem()
            item.setText(u'{} ({})'.format(user, len(self.data[user])))
            item.setToolTip(u'{}, тел. {}'.format(user.name, user.phone))
            item.setData(QtCore.Qt.UserRole, user)
            self.table.setVerticalHeaderItem(n, item)

        color = QtGui.QColor('#57b2ff')

        for n, date in enumerate(res.datespan(self.from_date(), self.today())):
            item = QtGui.QTableWidgetItem()
            item.setText(date.strftime(u'%-d'))
            item.setData(QtCore.Qt.UserRole, date)
            self.table.setHorizontalHeaderItem(n, item)

            for m, user in enumerate(users):

                item = QtGui.QTableWidgetItem()

                if user in self.data and date in self.data[user]:
                    item.setCheckState(QtCore.Qt.Checked)
                elif date == today:
                    item.setCheckState(QtCore.Qt.Unchecked)

                if date == today:
                    item.setBackgroundColor(color)
                else:
                    item.setFlags(item.flags() & ~QtCore.Qt.ItemIsEnabled)

                self.table.setItem(m, n, item)

        self.table.itemChanged.connect(self.onItemClicked)

    @staticmethod
    def today():
        return datetime.date.today() - datetime.timedelta(days=0)

    def from_date(self):
        return self.today() - datetime.timedelta(days=self.today().day-1)

    def onItemClicked(self, item):
        user = self.table.verticalHeaderItem(item.row()).data(QtCore.Qt.UserRole).toPyObject()
        date = self.table.horizontalHeaderItem(item.column()).data(QtCore.Qt.UserRole).toPyObject()

        self.table.setCurrentItem(item)

        # разрешаем редактирование только текущей даты
        if date != self.today():
            return

        item.setCheckState([QtCore.Qt.Checked, QtCore.Qt.Unchecked][item.checkState() != QtCore.Qt.Checked])

        if item.checkState() == QtCore.Qt.Checked:
            model, created = models.SalaryCharge.objects.get_or_create(user=user, date=date)
            model.save()
            if created:
                self.data[user].append(date)
        else:
            model = models.SalaryCharge.objects.filter(user=user, date=date)
            model.delete()
            if date in self.data[user]:
                self.data[user].remove(date)

        self.table.verticalHeaderItem(item.row()).setText(u'{0} ({1})'.format(user, len(self.data[user])))
