# coding: utf-8

import re
import os
import logging
import threading
import collections


class ImageCache(collections.defaultdict):

    def __init__(self):
        super(ImageCache, self).__init__(set)
        self.is_loaded = False
        self.pattern = r'([0-9]+?)\.'
        self._worker = None

    def _read_directory(self, path, pattern):
        pattern = pattern or self.pattern
        regex = re.compile(pattern)

        if not os.access(path, os.R_OK):
            logging.warning('Access denied to directory "{}"'.format(path))
            return

        for filename in os.listdir(path):
            match = regex.match(filename)
            if match:
                key = long(match.group(1))
                self[key].add(filename)

        self.is_loaded = True
        logging.debug('Image cash loaded. {} items'.format(len(self)))

        if not os.access(path, os.W_OK):
            logging.warning('Images directory "{}" is not writable.'.format(path))

    def load(self, path, pattern=None):
        self._worker = threading.Thread(target=self._read_directory, args=(path, pattern))
        self._worker.start()

    def delete(self, key):
        del self[key]

if __name__ == '__main__':
    import sys

    if len(sys.argv) < 2:
        print 'Usage: {} directory'.format(os.path.basename(sys.argv[0]))
        exit()

    path = sys.argv[1]
    if not os.path.exists(path):
        print 'Directory {} does not exists'.format(path)

    images = ImageCache()
    images.load(path)
