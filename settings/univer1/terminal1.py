# coding: utf-8

place = 3
card_terminal_id = 8

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'shop',
        'HOST': '192.168.0.1',
        'USER': 'terminal1',
        'PASSWORD': '935R9nMWJ8',
        'OPTIONS': {'init_command': 'SET storage_engine=INNODB', 'connect_timeout': 3}
    },
}

printer = dict(
    url='file:/dev/usb/lp0',
    classname='printer.ESCPOS',
    charset='cp866',
    row_length=32,
    feed_length=2
)

scanner = dict(
    url='serial:/dev/ttyUSB0',
    classname='scanner.Scanner',
    prefix='\x00',
    suffix='\r\n',
)

camera = dict(
    classname='camera.Camera',
    url='/dev/video0',
    camera_index=-1,
)
