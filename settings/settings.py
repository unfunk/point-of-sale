# coding: utf-8

import os
import logging

DEBUG = True

PROJECT_PATH = os.path.realpath(os.path.join(os.path.dirname(__file__), os.pardir))

SECRET_KEY = 'UNIl+rf3y0t3jxdh8sz!p#o$k9=-*n0wgc)ep5ia7ita0b@%^+#=b'

_LOGGING = {
    'disable_existing_loggers': False,
    'version': 1,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'django.db': {},
    },
}

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.gzip.GZipMiddleware',
)

ROOT_URLCONF = 'urls'
STATIC_URL = '/static/'

STATICFILES_DIRS = ('res/css/', )
TEMPLATE_DIRS = (os.path.join(PROJECT_PATH, 'templates'), )

ALLOWED_HOSTS = ['*']

app_label = 'shop'
locale = 'ru_RU'

shop_name = u'Торговый дом Универ'
shop_url = u'td-univer.com.ua'
shop_phone = u'(097) 93-29-222'

currency = u'грн'
daemon_port = 58888

max_discount = 25

fullscreen = True
print_receipt_prompt = False

logging.basicConfig(
    level=[logging.INFO, logging.DEBUG][DEBUG],
    format='%(asctime)s %(message)s',
    datefmt='%d/%m/%Y %H:%M:%S',
)
