# coding: utf-8

import os
import socket
import logging
import argparse
import importlib

import django
import django.conf
from django.db import DEFAULT_DB_ALIAS
from django.conf import global_settings


settings = None


class SettingsWrapper:

    _settings = django.conf.settings

    def __init__(self, default_settings):
        self.configure(default_settings)

    def configure(self, config):
        self.__dict__['_settings'] = config

    def __setattr__(self, key, value):
        setattr(self._settings, key, value)

    def __getattr__(self, item):
        return getattr(self._settings, item)

    def __dir__(self):
        return dir(self.__dict__['_settings'])

    def items(self):
        return self.__dict__['_settings']._settings.__dict__.items()

    def get(self, item, default=None):
        return getattr(self._settings, item, default)

    def update(self, item, value):
        return setattr(self._settings, item, value)

    @staticmethod
    def get_all_databases():
        dbs = {}
        for d in os.listdir(__name__):
            m = os.path.join(__name__, d)
            if os.path.isdir(m) and (os.path.exists(os.path.join(m, '__init__.pyc')) or os.path.exists(os.path.join(m, '__init__.py'))):
                m = __import__(d, globals(), locals(), fromlist=['DATABASES'])
                if not hasattr(m, 'DATABASES'):
                    continue

                databases = getattr(m, 'DATABASES')

                if not databases or DEFAULT_DB_ALIAS not in databases:
                    continue

                dbs[d] = databases[DEFAULT_DB_ALIAS]

        return dbs

    def load_all_databases(self):
        databases = self.get_all_databases()
        databases[DEFAULT_DB_ALIAS] = settings.DATABASES[DEFAULT_DB_ALIAS]
        settings.DATABASES = databases
        django.db.connections.databases = databases

    def load(self, *path):
        m = importlib.import_module('.'.join(path))

        for key, value in m.__dict__.items():
            if key.startswith('__'):
                continue

            if hasattr(self._settings, key) and type(value) is tuple:
                value = getattr(self._settings, key) + value

            setattr(self._settings, key, value)


if not django.conf.settings.configured:

    try:
        hostname, domain = socket.getfqdn().split('.', 1)
    except ValueError:
        hostname = socket.gethostname()
        domain = hostname
        logging.warning('Domain is not set')

    parser = argparse.ArgumentParser()
    parser.add_argument('--domain', default=domain)
    parser.add_argument('--hostname', default=hostname)
    parser.add_argument('--mode', default='pos')
    parser.add_argument('--place')

    args = parser.parse_args()

    wrapper = SettingsWrapper(global_settings)
    wrapper.load(__name__, __name__)
    wrapper.load(__name__, args.domain)
    wrapper.load(__name__, args.domain, args.hostname)

    if args.place:
        wrapper.place = args.place

    django.conf.settings.configure(wrapper)
    django.setup()
    settings = SettingsWrapper(django.conf.settings)

    settings.args = args

    logging.info('Using settings for %s %s', args.domain, args.hostname)

