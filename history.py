# coding: utf-8

from PyQt4 import QtCore, QtGui

import res
import async
import models
from settings import settings


class HistoryTable:

    places = []

    sum_color = QtGui.QColor('#fffc6d')
    highlight_color = QtGui.QColor('#D7C7CD')

    _worker_documents = None
    _worker_stats = None

    def __init__(self, parent):
        self.parent = parent
        self.ui = parent.ui

        self.icon_card = QtGui.QIcon(QtGui.QPixmap(':/main/icons/card_64.png'))
        self.icon_cash = QtGui.QIcon(QtGui.QPixmap(':/main/icons/money_64.png'))

        res.setColumnsWidth(self.ui.tableDocuments, (100, 250, 100, 100, 500))
        self.ui.tableDocuments.horizontalHeader().setResizeMode(4, QtGui.QHeaderView.Stretch)
        parent.historyUpdate.connect(self.show)

        self.places = filter(None, (settings.cash, settings.card_terminal))

    @staticmethod
    def get_documents(places):
        return places[0].documents(places=places)

    @staticmethod
    def get_stats(places):
        return {i.id: i.stat() for i in places}

    def show(self):
        self._worker_documents = async.AsyncTask(self.get_documents, self.update_documents, args=(self.places, ))
        self._worker_stats = async.AsyncTask(self.get_stats, self.update_stats, args=(self.places, ))

    def update_documents(self, documents):
        table = self.ui.tableDocuments
        table.clearContents()
        table.setRowCount(documents.count())

        for n, doc in enumerate(documents):
            item_id = QtGui.QTableWidgetItem(str(doc.pk))
            item_id.setTextAlignment(QtCore.Qt.AlignRight)
            item_id.setData(QtCore.Qt.UserRole, doc)
            item_id.setIcon(self.icon_card if doc.cash.cashless else self.icon_cash)

            item_name = QtGui.QTableWidgetItem(u'{}'.format(doc.get_name()))
            item_time = QtGui.QTableWidgetItem(doc.get_time())
            item_time.setTextAlignment(QtCore.Qt.AlignCenter)

            item_sum = QtGui.QTableWidgetItem(u'{:.2f}'.format(doc.total_sum))
            item_sum.setTextAlignment(QtCore.Qt.AlignRight)
            item_sum.setBackgroundColor(self.sum_color)

            item_comment = QtGui.QTableWidgetItem(doc.get_comment())

            if doc.type != models.Document.DOC_SALE:
                item_id.setBackgroundColor(self.highlight_color)
                item_name.setBackgroundColor(self.highlight_color)
                item_time.setBackgroundColor(self.highlight_color)
                item_sum.setBackgroundColor(self.highlight_color)
                item_comment.setBackgroundColor(self.highlight_color)

            res.set_row_items(
                table,
                n,
                item_id,
                item_name,
                item_time,
                item_sum,
                item_comment
            )

        table.setCurrentCell(0, 1)

    def update_stats(self, stat):
        places = self.places
        stat_cash = stat[places[0].id]

        fmt_total = u'Итого: {sum_total:,.2f} ' + settings.currency

        self.ui.groupBoxStatCash.setTitle(unicode(places[0]))
        self.ui.labelCashStat.setText(
            u'Внесено: {sum_in:,.2f}     '
            u'Возвраты: {sum_refund:,.2f}     '
            u'Снято: {sum_out:,.2f}     '
            u'Чеки: {sum_sale:,.2f}'.format(**stat_cash))
        self.ui.labelCashTotal.setText(fmt_total.format(**stat_cash))

        if len(places) > 1:
            stat_card = stat[places[1].id]

            self.ui.groupBoxStatCard.setTitle(unicode(places[1]))
            self.ui.labelCardStat.setText(
                u'Возвраты: {sum_refund:,.2f}     '
                u'Снято: {sum_out:,.2f}     '
                u'Чеки: {sum_sale:,.2f}'.format(**stat_card))
            self.ui.labelCardTotal.setText(fmt_total.format(**stat_card))
        else:
            self.ui.groupBoxStatCard.setVisible(False)
