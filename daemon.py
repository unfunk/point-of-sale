#!/usr/bin/env python

import tornado.web
import tornado.wsgi
import tornado.ioloop
import tornado.httpserver

import django
import django.core.handlers.wsgi

import devices
from settings import settings


def main():
    settings.load_all_databases()

    devices.init(settings)

    container = tornado.wsgi.WSGIContainer(django.core.handlers.wsgi.WSGIHandler())

    static_url = settings.STATIC_URL
    static_dirs = settings.STATICFILES_DIRS

    app = tornado.web.Application([
        (static_url + 'css/(.*)', tornado.web.StaticFileHandler, {'path': static_dirs[0]}),
        (static_url + 'i/(.*)', tornado.web.StaticFileHandler, {'path': static_dirs[1]}),
        (r'.*', tornado.web.FallbackHandler, {'fallback': container}),
    ])

    ssl_options={
        'certfile': 'certs/server.crt',
        'keyfile': 'certs/server.key',
    }
    ssl_options = None

    http_server = tornado.httpserver.HTTPServer(app, ssl_options=ssl_options)
    http_server.listen(port=settings.daemon_port)

    try:
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        pass

if __name__ == '__main__':
    main()