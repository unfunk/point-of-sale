# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'res/invoice.ui'
#
# Created: Wed Dec 16 16:41:43 2015
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(914, 480)
        Dialog.setWindowTitle(_fromUtf8(""))
        Dialog.setModal(True)
        self.verticalLayout_2 = QtGui.QVBoxLayout(Dialog)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.labelCash = QtGui.QLabel(Dialog)
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.labelCash.setFont(font)
        self.labelCash.setText(_fromUtf8(""))
        self.labelCash.setAlignment(QtCore.Qt.AlignCenter)
        self.labelCash.setObjectName(_fromUtf8("labelCash"))
        self.verticalLayout.addWidget(self.labelCash)
        self.labelTitle = QtGui.QLabel(Dialog)
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.labelTitle.setFont(font)
        self.labelTitle.setText(_fromUtf8(""))
        self.labelTitle.setAlignment(QtCore.Qt.AlignCenter)
        self.labelTitle.setObjectName(_fromUtf8("labelTitle"))
        self.verticalLayout.addWidget(self.labelTitle)
        self.tableProducts = QtGui.QTableWidget(Dialog)
        self.tableProducts.setEditTriggers(QtGui.QAbstractItemView.AnyKeyPressed|QtGui.QAbstractItemView.DoubleClicked|QtGui.QAbstractItemView.EditKeyPressed)
        self.tableProducts.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.tableProducts.setObjectName(_fromUtf8("tableProducts"))
        self.tableProducts.setColumnCount(6)
        self.tableProducts.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.tableProducts.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.tableProducts.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.tableProducts.setHorizontalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.tableProducts.setHorizontalHeaderItem(3, item)
        item = QtGui.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        self.tableProducts.setHorizontalHeaderItem(4, item)
        item = QtGui.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        item.setFont(font)
        self.tableProducts.setHorizontalHeaderItem(5, item)
        self.tableProducts.verticalHeader().setVisible(False)
        self.tableProducts.verticalHeader().setDefaultSectionSize(20)
        self.verticalLayout.addWidget(self.tableProducts)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.labelTotal = QtGui.QLabel(Dialog)
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.labelTotal.setFont(font)
        self.labelTotal.setText(_fromUtf8(""))
        self.labelTotal.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.labelTotal.setObjectName(_fromUtf8("labelTotal"))
        self.horizontalLayout.addWidget(self.labelTotal)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.lineEditComment = QtGui.QLineEdit(Dialog)
        self.lineEditComment.setMinimumSize(QtCore.QSize(0, 0))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(False)
        font.setWeight(50)
        self.lineEditComment.setFont(font)
        self.lineEditComment.setMaxLength(100)
        self.lineEditComment.setObjectName(_fromUtf8("lineEditComment"))
        self.horizontalLayout_2.addWidget(self.lineEditComment)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.buttonOk = QtGui.QPushButton(Dialog)
        self.buttonOk.setMinimumSize(QtCore.QSize(0, 40))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.buttonOk.setFont(font)
        self.buttonOk.setObjectName(_fromUtf8("buttonOk"))
        self.horizontalLayout_2.addWidget(self.buttonOk)
        self.buttonPrint = QtGui.QPushButton(Dialog)
        self.buttonPrint.setMinimumSize(QtCore.QSize(0, 40))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.buttonPrint.setFont(font)
        self.buttonPrint.setObjectName(_fromUtf8("buttonPrint"))
        self.horizontalLayout_2.addWidget(self.buttonPrint)
        self.buttonClose = QtGui.QPushButton(Dialog)
        self.buttonClose.setMinimumSize(QtCore.QSize(0, 40))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.buttonClose.setFont(font)
        self.buttonClose.setObjectName(_fromUtf8("buttonClose"))
        self.horizontalLayout_2.addWidget(self.buttonClose)
        self.horizontalLayout_2.setStretch(0, 5)
        self.horizontalLayout_2.setStretch(2, 1)
        self.horizontalLayout_2.setStretch(3, 1)
        self.horizontalLayout_2.setStretch(4, 1)
        self.verticalLayout_2.addLayout(self.horizontalLayout_2)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        item = self.tableProducts.horizontalHeaderItem(0)
        item.setText(_translate("Dialog", "№", None))
        item = self.tableProducts.horizontalHeaderItem(1)
        item.setText(_translate("Dialog", "Код", None))
        item = self.tableProducts.horizontalHeaderItem(2)
        item.setText(_translate("Dialog", "Артикул", None))
        item = self.tableProducts.horizontalHeaderItem(3)
        item.setText(_translate("Dialog", "Наименование", None))
        item = self.tableProducts.horizontalHeaderItem(4)
        item.setText(_translate("Dialog", "Кол-во", None))
        item = self.tableProducts.horizontalHeaderItem(5)
        item.setText(_translate("Dialog", "Цена", None))
        self.lineEditComment.setPlaceholderText(_translate("Dialog", "Комментарий", None))
        self.buttonOk.setText(_translate("Dialog", "ОК", None))
        self.buttonPrint.setText(_translate("Dialog", "Печать", None))
        self.buttonClose.setText(_translate("Dialog", "Закрыть", None))

