# -*- coding: utf-8 -*-

import datetime
from PyQt4 import QtCore, QtGui

import login
import countinput
import cashout
import mainform
import sale
import refund
import move
import inventory
import invoice
from settings import settings

def singleton(class_):
    instances = {}

    def get_instance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]
    return get_instance

def setColumnsWidth(table, list_of_width):
    for n, width in zip(range(table.columnCount()), list_of_width):
        table.setColumnWidth(n, width)


def set_row_items(table, row, *items):
    for column, item in enumerate(items):
        if item is not QtGui.QTableWidgetItem:
            item = QtGui.QTableWidgetItem(item)
        table.setItem(row, column, item)


def getLabelFloat(label):
    value, ok = label.text().toDouble()
    return value if ok else 0.0


def getLabelText(label):
    return unicode(label.text().trimmed().toUtf8(), 'utf-8')


def get_datetime(datetime):
        dt = QtCore.QDateTime(datetime)
        return unicode(dt.toString('dd MMMM yyyy').toUtf8(), 'utf-8'), unicode(dt.toString('HH:mm:ss').toUtf8(), 'utf-8')


def datespan(start, end, delta=datetime.timedelta(days=1)):
    current = start
    while current <= end:
        yield current
        current += delta