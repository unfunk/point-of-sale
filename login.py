# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui

import res
from settings import settings
import models


class LoginDialog(QtGui.QDialog):

    user = None
    cash = None

    def __init__(self, parent):
        super(LoginDialog, self).__init__(parent)
        self.ui = res.login.Ui_Dialog()
        self.ui.setupUi(self)
        self.parent = parent
        self.cash = settings.cash
        self.user = None

        self.setFixedSize(self.size())

        users = (models.User(),) + tuple(models.User.objects.filter(role=5).only('id', 'login').order_by('login'))

        for user in users:
            self.ui.comboUsers.addItem(unicode(user), QtCore.QVariant(user))

        last_user = self.cash.last_user()
        if last_user in users:
            self.ui.comboUsers.setCurrentIndex(users.index(last_user))

        self.ui.buttonLogin.clicked.connect(self.check_password)

    def check_password(self):
        index = self.ui.comboUsers.currentIndex()
        password = unicode(self.ui.editPassword.text())

        user = self.ui.comboUsers.itemData(index).toPyObject()

        try:
            user = models.User.objects.get(pk=user.pk, password=password)
        except models.User.DoesNotExist:
            self.ui.editPassword.clear()
            QtGui.QMessageBox.about(self, u'Сообщение', u'<h2>Неправильный пароль!</h2>')
            return

        user.last_cash = self.cash
        user.save(update_fields=['last_login', 'last_cash'])

        self.user = user
        self.accept()

    @staticmethod
    def get_user(parent=None):
        dialog = LoginDialog(parent)
        result = dialog.exec_()
        return dialog.user, result == QtGui.QDialog.Accepted