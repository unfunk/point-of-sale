import datetime

from django.db.models import Sum
from django.http import HttpResponse
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from django.db import connections, DEFAULT_DB_ALIAS

from SimpleXMLRPCServer import SimpleXMLRPCDispatcher

import models
from settings import settings

dispatcher = SimpleXMLRPCDispatcher(allow_none=True, encoding=None)

def rpc_method(func):
    dispatcher.register_function(func, func.__name__)
    return func


def get_db_override(cash_id):
    db_overrides = {
        6: 'univer2',
        7: 'univer3',
    }
    return db_overrides.get(int(cash_id), DEFAULT_DB_ALIAS)


class RpcHandler(View):

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        super(RpcHandler, self).dispatch(request, *args, **kwargs)
        if len(request.body):
            response = HttpResponse(content_type='application/xml')
            response.write(dispatcher._marshaled_dispatch(request.body))
        else:
            response = HttpResponse()
            response.write('<b>This is an XML-RPC Service.</b><br/>')
            response.write('The following methods are available:<ul>')

            for method in dispatcher.system_listMethods():
                help = dispatcher.system_methodHelp(method)
                response.write('<li><b>{0}</b>: {1}</li>'.format(method, help))
            response.write('</ul>')
        response['Content-Length'] = str(len(response.content))

        return response

@rpc_method
def printer_start(label_type, data):
    """Start label printing"""
    p = settings.label_printer

    for i in data:
        #if 'old_price' not in i:
        #    label_type = 3

        # remove duplicate spaces
        i['name'] = ' '.join(i['name'].split())

        i['price'] = float(i['price'])
        i['old_price'] = float(i['old_price'])
        i['shop_name'] = i['shop_name'] or settings.shop_name

        label = p.create_label(**i)

        labels = {
            label.LABEL_SMALL: p.print_small_label,
            label.LABEL_BIG: p.print_big_label,
            label.LABEL_BIG_DISCOUNT: p.print_big_label_discount,
        }

        labels.get(label_type, labels[label.LABEL_SMALL])(label)
    p.start()

@rpc_method
def printer_init():
    """Initialize printer"""
    #p = get_printer()
    import os
    p = settings.label_printer

    p.set_rate(4.0)

    os.chdir(settings.PROJECT_PATH)
    p.download_fonts()

@rpc_method
def print_labels(label_type, items, shop_name=None):
    printer = settings.label_printer

    Label = printer.Label

    labels = {
        Label.LABEL_SMALL: printer.print_small_label,
        Label.LABEL_BIG: printer.print_big_label,
        Label.LABEL_BIG_DISCOUNT: printer.print_big_label_discount,
    }
    print_label_method = labels.get(label_type, printer.print_small_label)

    for i in items:
        i['shop_name'] = i['shop_name'] or shop_name or settings.shop_name
        print_label_method(Label(**i))
        printer.set_quantity(i['quantity'])
    printer.start()

    # TODO replace to another thread
    for i in items:

        try:
            product = models.Product.objects.get(pk=i['id'])
        except models.Product.DoesNotExist:
            product = models.Product(
                pk=i['id'],
                category_id=i.get('category', 0),
                price=i.get('price', 0),
                stock=i.get('stock', 0)
            )

        product.name = i['name']
        product.article = i['article']
        product.category_id = i['category']
        product.price = i['price']
        product.save()


@rpc_method
def get_document_date(pk):
    return models.Document.objects.get(pk=pk).get_date()

@rpc_method
def get_document_comment(pk):
    return models.Document.objects.get(pk=pk).get_comment()

@rpc_method
def get_inventory_products(document):
    """Load products in document"""
    return list(models.ProductInventory.objects.filter(document=document, count__gt=0).values('product', 'count'))

@rpc_method
def get_invoice_products(pk):
    return list(models.ProductInvoice.objects.filter(document=pk, count__gt=0).values('product', 'count'))

@rpc_method
def get_move_products(document):
    """Load movement document"""
    return list(models.ProductMove.objects.filter(document=document, count__gt=0).values('product', 'count'))

@rpc_method
def update_product(id, name, article, barcode, brand, gender, season, country, product_type, size, color, category, price, currency, stock):
    """Update product's properties"""
    category = models.Category.objects.get(pk=category)
    brand = models.Brand.objects.get(pk=brand) or 0
    args = locals()
    product, created = models.Product.objects.get_or_create(pk=id)

    for key, value in args.iteritems():
        setattr(product, key, value)

    product.save()

@rpc_method
def update_brand(id, name):
    """Update brand"""
    brand, created = models.Brand.objects.get_or_create(pk=id)
    brand.name = name
    brand.save()


@rpc_method
def document_load():
    """Load document from csv file"""
    import csv

    data = []

    for line in csv.reader(open('/home/share/3.csv'), delimiter='\t'):
        if not line[0]:
            continue

        data.append(dict(
            article=line[0].split('/')[0].strip(),
            name=line[1],
            count=line[2],
            price=line[3],
        ))

    return data


@rpc_method
def set_document_active(pk, is_active):
    """Set document active flag"""
    try:
        doc = models.Document.objects.get(pk=pk)
        doc.is_active = is_active
        doc.save()
    except models.Document.DoesNotExist:
        pass


@rpc_method
def cash_stat(pk):
    """Get cash balance"""
    db = get_db_override(pk)

    obj = models.Cash.objects.using(db).get(pk=pk)
    stat = obj.stat()
    for k, v in stat.items():
        stat[k] = round(v, 2)

    return dict(
        id=obj.pk,
        name=obj.name,
        count=obj.count(),
        sum_in=stat['sum_in'],
        sum_out=stat['sum_out'],
        sum_sale=stat['sum_sale'],
        sum_refund=stat['sum_refund']
    )


@rpc_method
def upload_products(items):
    """Sync products"""
    print items.split(',')


@rpc_method
def get_expenses(cash_id):

    db = get_db_override(cash_id)

    items = []

    try:
        cash = models.Cash.objects.using(db).get(pk=cash_id)
    except models.Cash.DoesNotExist:
        return items

    for i in cash.expenses():
        items.append(dict(
            id=i.pk,
            time=i.get_time(),
            total_sum=i.total_sum,
            comment=i.get_comment()
        ))

    return items

@rpc_method
def get_sales(cash_id, period_start, period_end):
    db = get_db_override()


def get_documents_cash(document_type, cash_id):
    qs = document_type.objects.filter(is_active=True, cash=cash_id)

    documents = []
    for doc in qs:
        documents.append(dict(
            id=doc.id,
            cash=doc.cash.pk,
            date=doc.get_date('%Y%m%d'),
            time=doc.get_time(),
            total_sum=doc.total_sum,
            comment=doc.get_comment(),
        ))

    return documents


@rpc_method
def get_documents_cashout(cash_id):
    return get_documents_cash(models.DocumentCashOut, cash_id)


@rpc_method
def get_documents_cashin(cash_id):
    return get_documents_cash(models.DocumentCashIn, cash_id)


@rpc_method
def get_active_dates(cash_id):
    qs = models.DocumentSale.objects.filter(is_active=True, cash=cash_id).dates('date', 'day')
    return [dt.strftime('%Y%m%d') for dt in qs]


@rpc_method
def get_sales_by_date(cash_id, date):
    date = datetime.datetime.strptime(date, '%Y%m%d')

    qs = models.ProductSale.objects.filter(
        document__date__year=date.year,
        document__date__month=date.month,
        document__date__day=date.day,
        document__is_active=True,
        document__cash__id=cash_id,
    ).select_related('document', 'product__id')

    items = []
    for i in qs:
        items.append(dict(
            document_id=i.document.pk,
            date=i.document.get_date(),
            time=i.document.get_time(),
            comment=i.document.get_comment(),
            price=i.price,
            discount=i.discount,
            count=i.count,
            cost=i.cost,
            product_id=i.product_id,
        ))

    return items

@rpc_method
def upload_cash_place(cash_id, name, cashless):
    cash, created = models.Cash.objects.get_or_create(pk=cash_id)
    cash.name = name
    cash.cashless = bool(cashless)
    cash.save()
