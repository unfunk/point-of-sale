# -*- coding: utf-8 -*-

import os
import logging

from PyQt4 import QtCore, QtGui

import res
from settings import settings


class CountInput(QtGui.QDialog):

    parent = None
    capture = None
    model = None
    image = None
    count = 0
    callback = None

    def __init__(self, parent, model, count=0, callback=None):
        super(CountInput, self).__init__()
        ui = res.countinput.Ui_Dialog()
        self.ui = ui
        self.ui.setupUi(self)

        self.parent = parent
        self.model = model
        self.callback = callback
        self.count = count

        self.setWindowModality(QtCore.Qt.ApplicationModal)

        ui.labelId.setText(unicode(model.pk))
        ui.labelTitle.setText(unicode(model))
        ui.labelArticle.setText(model.article)
        ui.labelStock.setText(u'{} шт.'.format(model.stock))
        ui.labelCategory.setText(unicode(model.category))

        self.graphicsScene = QtGui.QGraphicsScene(self)
        ui.graphicsViewImage.setScene(self.graphicsScene)
        self.graphicsViewSize = ui.graphicsViewImage.size()

        self.camera = settings.camera
        if not self.camera:
            ui.pushButtonAddPhoto.setDisabled(True)

        if model.stock <= 0:
            ui.labelStock.setStyleSheet('color: red')

        ui.labelPrice.setText(model.get_price())
        ui.pushButton.clicked.connect(self.onOk)
        ui.pushButtonAddPhoto.clicked.connect(self.photo_capture_start)
        ui.pushButtonDelPhoto.clicked.connect(self.photo_delete)
        ui.pushButtonCapture.clicked.connect(self.photo_save)
        ui.pushButtonCancel.clicked.connect(self.photo_capture_cancel)

        ui.spinBox.setFocus()
        ui.spinBox.setValue(self.count)
        ui.spinBox.selectAll()

        ui.spinBox.keyPressEvent = self.spinBoxKeyPressEvent

        self.photo_capture_cancel()

    def onOk(self):
        self.callback(self.model, self.ui.spinBox.value())
        self.close()

    def keyPressEvent(self, e):
        key = e.key()

        if key == QtCore.Qt.Key_Escape:
            self.close()
        elif key == QtCore.Qt.Key_Space:
            self.keySpacePressEvent()

        QtGui.QDialog.keyPressEvent(self, e)

    def closeEvent(self, e):
        if self.camera:
            self.camera.stop()

    def spinBoxKeyPressEvent(self, event):
        key = event.key()

        if key == QtCore.Qt.Key_Space:
            self.keySpacePressEvent()
            return

        return QtGui.QSpinBox.keyPressEvent(self.ui.spinBox, event)

    def keySpacePressEvent(self):
        if self.ui.pushButtonAddPhoto.isVisible():
            self.ui.pushButtonAddPhoto.click()
        else:
            self.ui.pushButtonCapture.click()

    def photo_set(self, image):
        self.graphicsScene.clear()
        self.graphicsScene.addPixmap(QtGui.QPixmap(image))

    def photo_load(self):
        photo, path, directory = self.model.get_image()
        if not photo:
            path = directory + '/0.jpg'
            self.ui.pushButtonDelPhoto.setDisabled(True)
        else:
            self.ui.pushButtonDelPhoto.setDisabled(False)
            self.parent.product_images[self.model.pk].add(photo)

        self.photo_set(QtGui.QPixmap(path).scaled(
            self.ui.graphicsViewImage.size(),
            QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation
        ))

    def photo_delete(self):
        photo, path, directory = self.model.get_image()
        if photo:
            os.remove(path)
            self.parent.product_images.delete(self.model.pk)

        self.photo_load()

    def photo_save(self):
        self.camera.stop()
        self.camera.save(self.model.get_image()[1])
        self.photo_capture_cancel()

    def photo_capture_cancel(self):
        if self.camera:
            self.camera.stop()

        self.photo_load()

        self.ui.pushButtonCapture.setVisible(False)
        self.ui.pushButtonCancel.setVisible(False)
        self.ui.pushButtonAddPhoto.setVisible(True)
        self.ui.pushButtonDelPhoto.setVisible(True)

    def photo_capture_start(self):
        self.ui.pushButtonCapture.setVisible(True)
        self.ui.pushButtonCancel.setVisible(True)
        self.ui.pushButtonAddPhoto.setVisible(False)
        self.ui.pushButtonDelPhoto.setVisible(False)

        self.graphicsViewSize = self.ui.graphicsViewImage.size()

        self.camera.setup(
            preview_width=self.ui.graphicsViewImage.size().width(),
            preview_height=self.ui.graphicsViewImage.size().height(),
            callback=self.photo_process_frame,
        )
        try:
            self.camera.start()
        except self.camera.CameraError, e:
            logging.error(e)
            self.photo_capture_cancel()

    def photo_process_frame(self, frame):
        if self.camera.is_active:
            self.photo_set(QtGui.QImage(frame[0], frame.shape[1], frame.shape[0], QtGui.QImage.Format_RGB888))
