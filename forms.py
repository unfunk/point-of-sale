from django import forms
from django.core.files import File
from PIL import Image
import StringIO

from settings import settings
import models

class UploadPictureForm(forms.Form):
    picture = forms.ImageField()

    def save(self):
        cleaned_data = self.cleaned_data
        pk = int(self.data.get('id'))
        product = models.Product.objects.get(pk=pk)
        
        img = Image.open(StringIO.StringIO(cleaned_data['picture'].read()))
        img.thumbnail((800, 600), Image.ANTIALIAS)
        output = file(product.get_image()[1], 'wb+')
        img.save(output, format='JPEG', quality=70)
        output.close()