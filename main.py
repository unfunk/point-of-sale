# -*- coding: utf-8 -*-

import logging

from django.db.models import Q
from django.db import transaction
from PyQt4 import QtCore, QtGui

from settings import settings
import models
import res
import cart
import image
import history
import journal
import documents
import countinput
import work_schedule


class MainForm(QtGui.QDialog):

    _carts = []
    _documents = {}
    _child_widgets = {}

    scanner = None

    productAdd = QtCore.pyqtSignal(models.Product, int, name='productAdd')
    historyUpdate = QtCore.pyqtSignal()
    journalUpdate = QtCore.pyqtSignal()

    product_images = image.ImageCache()

    def __init__(self, user):
        super(MainForm, self).__init__()

        self.product_image_icon = QtGui.QIcon(QtGui.QPixmap(':/main/icons/camera_64.png').scaled(32, 32))

        self.setupUi()

        self.scanner = settings.scanner
        if self.scanner:
            self.scanner.connect(self.onBarcodeScan)
        #    logging.warning('Barcode scanner is not configured')

        self.productAdd.connect(self.onProductAdd)

        self.updateLabels()

        if settings.fullscreen:
            self.showMaximized()
        else:
            self.show()

        self.product_images.load(settings.images_path)

    def setupUi(self):
        self.ui = res.mainform.Ui_Dialog()
        self.ui.setupUi(self)

        ui = self.ui

        self.setWindowTitle(u'{} / {} / {}'.format(settings.shop_name, settings.cash, settings.user))

        self.addCart()

        ui.tableCart.setWordWrap(True)
        ui.tableCart.horizontalHeader().setResizeMode(1, QtGui.QHeaderView.Stretch)
        ui.tableCart.itemChanged.connect(self.cartItemChanged)

        ui.tableCart.keyPressEvent = self.cartKeyPressEvent
        ui.tableCart.itemDoubleClicked.connect(self.cartItemClicked)

        ui.tableProducts.itemActivated.connect(self.onItemActivated)
        ui.tableProducts.horizontalHeader().setResizeMode(1, QtGui.QHeaderView.Stretch)
        ui.editCash.textChanged.connect(self.onChangeCash)

        ui.labelCashier.setText(unicode(settings.user))
        ui.labelPlace.setText(unicode(settings.cash))

        ui.buttonSale.clicked.connect(self.saleClick)
        ui.buttonSaleCard.clicked.connect(lambda: self.saleClick(card=True))

        ui.buttonCancel.clicked.connect(self.clear)

        ui.labelDate.setText(QtCore.QDate.currentDate().toString("dddd dd MMMM yyyy"))
        ui.tabWidget.currentChanged.connect(self.onChangeTab)

        ui.editSearch.returnPressed.connect(self.onSearchEntered)
        ui.buttonSearch.clicked.connect(self.onSearchEntered)
        ui.editSearch.setFocus()

        ui.buttonDocRefund.clicked.connect(self.createDocumentRefund)
        ui.buttonDocCashOut.clicked.connect(lambda: self.createDocument(documents.DocumentCashOut))
        ui.buttonDocMove.clicked.connect(lambda: self.createDocument(documents.DocumentMove))
        ui.buttonDocCashIn.clicked.connect(lambda: self.createDocument(documents.DocumentCashIn))
        ui.buttonInvoice.clicked.connect(lambda: self.createDocument(documents.DocumentInvoice, cart=self.cart))

        ui.buttonSaleCard.setEnabled(bool(settings.card_terminal))

        ui.tableDocuments.itemActivated.connect(self.openDocument)
        ui.tableJournal.itemActivated.connect(self.openDocument)

        ui.tabCarts.currentChanged.connect(self.onChangeCartTab)

        validator = QtGui.QDoubleValidator(0, 90000, 2, self)
        ui.editCash.setValidator(validator)

        ui.buttonAddCart.clicked.connect(self.addCart)

        self.timer = QtCore.QTimer()

        self.timer.timeout.connect(self.showTime)
        self.showTime()
        self.timer.start(60 * 1000)

        self.initCategories()

    def showChild(self, child):
        if not child:
            return

        self._child_widgets[child.__class__.__name__] = child
        child.show()

    def closeChild(self, child):
        child.hide()
        self._child_widgets.remove(child)

    def createDocument(self, widget, *args, **kwargs):
        w = widget(self, *args, **kwargs)
        self.showChild(w)

    def showTime(self):
        self.ui.labelTime.setText(QtCore.QTime.currentTime().toString("hh:mm"))

    def keyPressEvent(self, e):
        key = e.key()

        if key == QtCore.Qt.Key_Escape:
            self.close()
        elif key == QtCore.Qt.Key_F1:
            self.ui.tabWidget.setCurrentIndex(0)
            self.ui.editSearch.setFocus()
        elif key == QtCore.Qt.Key_F4:
            self.clear()
        elif key == QtCore.Qt.Key_F8:
            self.ui.tabWidget.setCurrentIndex(0)
            self.ui.buttonSaleCard.click()
        elif key == QtCore.Qt.Key_F9:
            self.ui.tabWidget.setCurrentIndex(0)
            self.ui.editCash.setFocus()
        elif key == QtCore.Qt.Key_F10:
            self.addCart()
        elif key == QtCore.Qt.Key_F12:
            self.ui.tabWidget.setCurrentIndex(0)
            self.ui.buttonSale.click()

    def cartKeyPressEvent(self, event):
        if not self.cart.is_empty():
            key = event.key()

            if key == QtCore.Qt.Key_Delete:
                self.cart.remove(self.getCartProduct().id)
                self.updateTableCart()
                self.ui.editSearch.setFocus()
            elif key == QtCore.Qt.Key_Plus:
                self.cart.add(self.getCartProduct(), 1)
                self.updateTableCart(self.getCartProduct().id)
            elif key == QtCore.Qt.Key_Minus:
                self.cart.add(self.getCartProduct(), -1)
                self.updateTableCart(self.getCartProduct().id)

        return QtGui.QTableWidget.keyPressEvent(self.ui.tableCart, event)

    def closeEvent(self, e):
        if len(self._carts) > 1 and self.cart.is_empty():
            self.clear()
            e.ignore()
            return

        if self.cart.is_empty():
            e.accept()
            return

        reply = QtGui.QMessageBox.question(self,
            u'Сообщение', u'<h2>Корзина покупателя не пуста!<br/>Выйти из программы?</h2>',
            QtGui.QMessageBox.Yes, QtGui.QMessageBox.No
        )

        if reply == QtGui.QMessageBox.Yes:
            e.accept()
        else:
            e.ignore()

    @property
    def cart(self):
        """Текущая корзина"""
        return self._carts[self.ui.tabCarts.currentIndex()]

    def getLabelValue(self, label):
        value, ok = label.text().toDouble()
        return value if ok else 0.0

    def addProductToCart(self, product, count):
        "Добавить товар в корзину"
        self.cart.add(product, count)
        self.updateTableCart(product.pk)

    def getCartProduct(self, n=None):
        "Возвращает товар в корзине по его номеру"
        if n is None:
            n = self.ui.tableCart.currentRow()
        return self.ui.tableCart.item(n, 0).data(QtCore.Qt.UserRole).toPyObject()

    def updateTableCart(self, selected_id=None):

        self.cart.calc()

        self.updateLabels()

        table = self.ui.tableCart
        table.clearContents()
        table.setRowCount(0)

        if self.cart.is_empty():
            return

        color = QtGui.QColor('#fffc6d')

        for product in self.cart.values():
            table.insertRow(0)

            item_id = QtGui.QTableWidgetItem(str(product.id))
            item_id.setData(QtCore.Qt.UserRole, product.product)
            item_id.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)

            item_name = QtGui.QTableWidgetItem(product.name)
            item_name.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)

            item_article = QtGui.QTableWidgetItem(product.article)
            item_article.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)

            item_count = QtGui.QTableWidgetItem(u'{:.0f}'.format(product.count))
            item_count.setBackgroundColor(color)
            item_count.setTextAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
            item_count.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable)

            item_price = QtGui.QTableWidgetItem(u'{:.2f}'.format(product.price))
            item_price.setTextAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
            item_price.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)

            item_sum = QtGui.QTableWidgetItem(u'{:.2f}'.format(product.sum))
            item_sum.setTextAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
            item_sum.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)

            item_discount = QtGui.QTableWidgetItem(u'{:.3f}'.format(product.discount))
            item_discount.setBackgroundColor(color)
            item_discount.setTextAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
            item_discount.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable)

            item_total = QtGui.QTableWidgetItem(u'{:.2f}'.format(product.total_sum))
            item_total.setTextAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
            item_total.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)

            res.set_row_items(table, 0,
                item_id,
                item_name,
                item_article,
                item_count,
                item_price,
                item_sum,
                item_discount,
                item_total
            )

            if product.id == selected_id:
                table.setCurrentCell(0, 1)


        table.resizeRowsToContents()

    def addCart(self):
        self.ui.editSearch.setFocus()

        # пустой чек не может быть отложен
        if len(self._carts) and self.cart.is_empty():
            return

        empty_cart = filter(lambda c: c.length() == 0, self._carts)

        index = 0

        if not empty_cart:
            self._carts.append(cart.Cart())
            w = QtGui.QWidget()
            index = self.ui.tabCarts.addTab(w, self.cart.name())
            w.setObjectName('tabCart_'+ str(index))
        else:
            index = self._carts.index(empty_cart[0])

        self.ui.tabCarts.setCurrentIndex(index)

    def updateLabels(self):
        ui = self.ui
        cart = self.cart
        is_empty = cart.is_empty()

        ui.labelTotal.setText('{:.2f}'.format(cart.total))
        ui.labelChange.setText('{:.2f}'.format(cart.change) if cart.change else '')

        ui.labelChange.setStyleSheet('QLabel { color: #ff2d16; }' if cart.change < 0 else '')

        if not cart.cash and is_empty:
            ui.editSearch.clear()

        ui.tabCarts.setTabText(ui.tabCarts.currentIndex(), cart.name())
        ui.buttonAddCart.setDisabled(is_empty)
        ui.buttonSale.setDisabled(is_empty)
        ui.buttonInvoice.setDisabled(is_empty)
        ui.buttonSaleCard.setDisabled(is_empty or not settings.card_terminal)

    def onBarcodeScan(self, barcode):
        self.ui.tabWidget.setCurrentIndex(0)
        self.show()

        products = self.searchProduct(barcode=barcode)
        if not products:
            self.message(u'Штрихкод "{}" не найден.'.format(barcode))
            return

        if len(products) == 1:
            self.productAdd.emit(products[0], 0)

        self.loadProducts(products=products)

    def onChangeCash(self):
        self.cart.cash = self.getLabelValue(self.ui.editCash)
        self.updateLabels()

    def onProductCountInput(self, product, count):
        self.cart[product.id].count = count
        self.updateTableCart()
        self.ui.editSearch.setFocus()

    def cartItemClicked(self, item):
        if item.column() not in [0, 1, 2]:
            return

        product = self.getCartProduct(item.row())
        count = self.cart[product.id].count
        dlg = countinput.CountInput(self, product, count, self.onProductCountInput)
        self.showChild(dlg)

    def cartItemChanged(self, item):

        columns = {
            3: 'count',
            6: 'discount',
        }

        col = item.column()

        if col not in columns:
            return

        product_id = self.getCartProduct(item.row()).id

        attr = columns[col]
        old_value = getattr(self.cart[product_id], attr)

        if col == 3:
            value, ok = item.text().toInt()

            if value == 0:
                ok = False

        elif col == 6:
            value, ok = item.text().toDouble()

            if value > settings.max_discount:
                ok = False

        if not ok:
            item.setText(unicode(old_value))
            return

        if old_value == value:
            return

        setattr(self.cart[product_id], attr, abs(value))
        self.updateTableCart()

    @staticmethod
    def message(text):
        QtGui.QMessageBox.warning(None, u'Сообщение', u'<h2>{}</h2>'.format(text))

    def onSearchEntered(self):
        value = self.ui.editSearch.text().trimmed()

        if not value:
            return

        code, ok = value.toULongLong()

        if len(value) == 13 and ok:
            products = self.searchProduct(barcode=code)
        elif len(value) < 6 and ok:
            products = self.searchProduct(id=code)
        else:
            products = self.searchProduct(search=unicode(value.toUtf8(), 'utf-8'))

        self.ui.editSearch.clear()

        if not products:
            self.message(u'Товар "{}" не найден.'.format(value))
            return

        if len(products) == 1:
            self.productAdd.emit(products[0], 0)

        self.loadProducts(products=products)

    def searchProduct(self, barcode=None, id=None, search=None):
        items = models.Product.objects.select_related('category')

        if barcode:
            items = items.filter(barcode=barcode)
        elif id:
            items = items.filter(pk=id)
        else:
            items = items.filter(Q(name__icontains=search) | Q(article__icontains=search))

        return items

    def loadProducts(self, category=None, products=None):
        table = self.ui.tableProducts
        table.clearContents()
        table.setRowCount(0)

        if category:
            products = models.Product.objects.select_related('category').filter(category=category)

        for i in products:
            table.insertRow(0)

            item_id = QtGui.QTableWidgetItem(str(i.pk))
            item_id.setTextAlignment(QtCore.Qt.AlignRight)
            item_id.setData(QtCore.Qt.UserRole, QtCore.QVariant(i))

            if i.pk in self.product_images:
                item_id.setIcon(self.product_image_icon)

            item_article = QtGui.QTableWidgetItem(i.article)
            item_article.setTextAlignment(QtCore.Qt.AlignCenter)

            item_stock = QtGui.QTableWidgetItem(str(i.stock))
            item_stock.setTextAlignment(QtCore.Qt.AlignCenter)
            if i.stock < 0:
                item_stock.setBackgroundColor(QtGui.QColor('#ff5261'))

            item_price = QtGui.QTableWidgetItem(i.get_price())
            font = item_price.font()
            font.setBold(True)
            item_price.setFont(font)
            item_price.setBackgroundColor(QtGui.QColor('#fffc6d'))
            item_price.setTextAlignment(QtCore.Qt.AlignRight)

            res.set_row_items(
                table,
                0,
                item_id,
                unicode(i),
                item_article,
                item_stock,
                item_price
            )

        table.setCurrentCell(0, 0)

    def onItemActivated(self, item):
        product = self.ui.tableProducts.item(item.row(), 0).data(QtCore.Qt.UserRole).toPyObject()

        self.productAdd.emit(product, 0)

    def onProductAdd(self, product, count):
        if not count:
            dlg = countinput.CountInput(self, product, callback=self.onProductAdd)
            self.showChild(dlg)
        else:
            self.cart.add(product, count)
            self.updateTableCart(product.pk)
            self.ui.editSearch.setFocus()

    def onCategoryChange(self, current, previous):
        category = current.data(1, QtCore.Qt.UserRole).toPyObject()
        self.loadProducts(category=category)

    def initCategories(self):
        data = list(models.Category.objects.all().order_by('name'))

        tree = self.ui.tree_categories
        tree.setColumnCount(1)

        tree.currentItemChanged.connect(self.onCategoryChange)

        def make_item(text, data):
            item = QtGui.QTreeWidgetItem()
            item.setText(0, text)
            item.setData(1, QtCore.Qt.UserRole, QtCore.QVariant(data))
            return item

        def make_items(data):
            l = []
            for i in data:
                l.append(make_item(unicode(i), i.pk))
            return l

        #roots = filter(lambda i: i.parent==0, data)
        roots = data
        top_items = make_items(roots)
        tree.addTopLevelItems(top_items)

        return
        for n in range(len(roots)):
            for i in data:
                if roots[n].pk == i.parent:
                    top_items[n].addChild(make_item(unicode(i), i))


    def printReceipt(self, document):
        try:
            documents.DocumentSale.to_print(document, self.cart)
        except Exception, msg:
            self.message(u'Не удалось подключиться к принтеру чеков.\n'+ str(msg).decode('utf-8'))

    def clear(self):
        index = self.ui.tabCarts.currentIndex()

        if self.cart.is_empty() and index > 0:
            self.ui.tabCarts.removeTab(index)
            self._carts.pop(index)
        else:
            self.cart.clear()

        self.ui.editCash.clear()
        self.updateTableCart()

        self.ui.editSearch.setFocus()

    def createDocumentSale(self, is_card=False):
        cash = settings.card_terminal if is_card else settings.cash

        doc = models.DocumentSale.create(settings.user, cash, self.cart)

        if settings.print_receipt_prompt:
            reply = QtGui.QMessageBox.question(
                self,
                u'Сообщение', u'Печатать чек?',
                QtGui.QMessageBox.Yes | QtGui.QMessageBox.No, QtGui.QMessageBox.No)

            if reply == QtGui.QMessageBox.Yes:
                self.printReceipt(doc)
        else:
            self.printReceipt(doc)

        return doc

    def createDocumentRefund(self):
        dialog = QtGui.QInputDialog(self)
        dialog.setInputMode(QtGui.QInputDialog.TextInput)
        dialog.setWindowTitle(u'Номер чека')
        dialog.setLabelText(u'<h1>Введите номер чека:</h1>')
        dialog.setStyleSheet('QLineEdit { font-size: 18pt; font-weight: bold; }')
        ok = dialog.exec_()
        value = dialog.textValue()

        if ok:
            try:
                doc = models.DocumentSale.objects.get(pk=value)
                self.showChild(documents.DocumentRefund(self, base_document=doc))
            except models.DocumentSale.DoesNotExist:
                self.message(u'Чек №{} не найден!'.format(value))
                return

    def saleClick(self, card=False):
        cart = self.cart

        if cart.is_empty():
            self.message(u'Корзина покупателя не содержит товаров.')
            return

        if card:
            cart.cash = cart.total

        if not cart.cash:
            self.message(u'Введите сумму полученных денег.')
            return

        if cart.cash < cart.total:
            self.message(u'Сумма полученных денег меньше общей суммы.')
            return

        self.createDocumentSale(card)
        self.clear()

    def onChangeTab(self):
        tab = self.ui.tabWidget.currentWidget().objectName()
        widget = None

        if tab == 'tabCash':
            widget = history.HistoryTable(self)
        elif tab == 'tabJournal':
            widget = journal.JournalTable(self)
        elif tab == 'tabWork':
            widget = work_schedule.WorkSchedule(self)
        elif tab == 'tabSales':
            pass

        self.showChild(widget)

    def onChangeCartTab(self):
        index = self.ui.tabCarts.currentIndex()
        name = self.ui.tabCarts.currentWidget().objectName()
        self.updateLabels()
        self.updateTableCart()

    def openDocument(self, item=None, doc=None):
        if not doc:
            if self.ui.tabWidget.currentWidget().objectName() == 'tabCash':
                table = self.ui.tableDocuments
            else:
                table = self.ui.tableJournal

            doc = table.item(item.row(), 0).data(QtCore.Qt.UserRole).toPyObject()

        types = {
            models.Document.DOC_CASH_OUT:   documents.DocumentCashOut,
            models.Document.DOC_CASH_IN:    documents.DocumentCashIn,
            models.Document.DOC_SALE:       documents.DocumentSale,
            models.Document.DOC_REFUND:     documents.DocumentRefund,
            models.Document.DOC_INVENTORY:  documents.DocumentInventory,
            models.Document.DOC_INVOICE:    documents.DocumentInvoice,
            models.Document.DOC_MOVE:       documents.DocumentMove,
        }

        document = types.get(doc.type)

        if document:
            model = document.model.objects.get(pk=doc.pk)
            w = document(self, model=model)
            self.showChild(w)
